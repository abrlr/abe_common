#/bin/sh

mkdir -p bin

LibraryFiles=

for file in `find abe_common/src -type f -name "*.c"`
do
	filenameandextension="${file##*/}"
	filename="${filenameandextension%.*}"
	echo ${filenameandextension}
    targetObjectFile="bin/${filename}.o"
    LibraryFiles+="${targetObjectFile} "
	gcc -I"." -DABE_LINUX -c ${file} -o ${targetObjectFile}
done

ar -rcs bin/libcommon.a bin/*.o