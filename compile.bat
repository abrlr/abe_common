@echo off
SETLOCAL EnableDelayedExpansion

if exist bin rmdir /s /q bin
if not exist bin mkdir bin

:: -Zi
set CompilerFlags=-DABE_WIN -nologo -I. -Fo"bin/"

set LibrarySources=
set LibraryObjs=
for /f %%a in ('dir /b /s abe_common\src\*.c') do (
	set LibrarySources=%%~a !LibrarySources!
	set LibraryObjs=bin\%%~na.obj !LibraryObjs!
)

cl /c %CompilerFlags% %LibrarySources%
lib %LibraryObjs% /out:bin/common.lib

:: -PDB:"bin/%%~na.pdb" -DEBUG
