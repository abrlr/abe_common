#ifndef AbeFormatsJsonInternal_h
#define AbeFormatsJsonInternal_h

#include "abe_common/include/formats/abe_json.h"

typedef struct JsonArrayInternal JsonArrayInternal;
typedef struct JsonObjectInternal JsonObjectInternal;
typedef struct JsonNodeInternal JsonNodeInternal;
typedef struct JsonDocumentInternal JsonDocumentInternal;

struct JsonArrayInternal {
	JsonNodeType valueType;
	u32 entryCount;
	JsonNodeInternal* entries[64];
};

struct JsonObjectInternal {
	u32 fieldCount;
	JsonNodeInternal* fields[64];
};

struct JsonNodeInternal {
	JsonNodeInternal* parent; // This is null only for the root json node (refered to as json document in api)
	JsonNodeInternal* next; // Used to save computations during write to stream
	MemoryArenaFixed* arena;

	JsonNodeType type;

	str8 key;
	union {
		f64 number;
		str8 string;
		b8 boolean;
		JsonArrayInternal array;
		JsonObjectInternal object;
	} value;
};

void json_internal_add_field_node(JsonNodeInternal* objectNode, JsonNodeInternal* fieldNode);
void json_internal_array_add_member_node(JsonNodeInternal* arrayNode, JsonNodeInternal* memberNode);

#endif // AbeFormatsJsonInternal_h