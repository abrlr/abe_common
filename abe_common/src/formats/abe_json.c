#include <stdio.h>

#include "abe_common/include/abe_log.h"
#include "abe_common/include/abe_memory.h"
#include "abe_common/include/formats/abe_json.h"

#include "abe_json_tokeniser.h"

global const char* g_JsonNodeTypenames[] = {
	"JsonNodeType_Invalid",
	"JsonNodeType_Number",
	"JsonNodeType_String",
	"JsonNodeType_Bool",
	"JsonNodeType_Array",
	"JsonNodeType_Object",
	"JsonNodeType_Null",
};

const char* json_typename(JsonNodeType type)
{
	return g_JsonNodeTypenames[type];
}

//- Json Documents

JsonDocument* json_document_create(MemoryArenaFixed* arena)
{
	JsonNodeInternal* result = MAFAllocStruct(arena, JsonNodeInternal, "JsonDocument");
	*result = (JsonNodeInternal)ZeroStruct;
	result->arena = arena;
	result->type = JsonNodeType_Object;

	return (JsonDocument*)result;
}

JsonDocument* json_document_create_from_stream(MemoryArenaFixed* arena, Stream* stream)
{
	JsonNodeInternal* result = json_tokeniser_generate_document_from_stream(arena, stream);
	if ( !result ) {
		log_err("Failed to create Json Document from stream.\n");
		goto out;
	}

out:
	return result;
}

JsonNode* json_document_create_child(JsonDocument* document, const char* key, JsonNodeType type)
{
	return json_node_create_child((JsonNode*)document, key, type);
}

b8 json_node_write_to_stream_recursive(Stream* stream, JsonNodeInternal* nodeToWrite, b8 pretty, i32 level);
b8 json_document_write_to_stream(JsonDocument* document, Stream* stream, b8 pretty)
{
	return json_node_write_to_stream_recursive(stream, (JsonNodeInternal*)document, pretty, 0);
}

//- Json Nodes

internal void json_internal_set_parent_child_relationship(JsonNodeInternal* parent, JsonNodeInternal* child)
{
	child->parent = parent;
	child->arena = parent->arena;

	return;
}

void json_internal_add_field_node(JsonNodeInternal* objectNode, JsonNodeInternal* fieldNode)
{
	json_internal_set_parent_child_relationship(objectNode, fieldNode);

	//- @TODO(abe) : Check that there is no overflow here

	// Sets the value of the previous field node's next pointer
	if ( objectNode->value.object.fieldCount ) {
		objectNode->value.object.fields[objectNode->value.object.fieldCount - 1]->next = fieldNode;
	}

	objectNode->value.object.fields[objectNode->value.object.fieldCount++] = fieldNode;

	return;
}

JsonNode* json_node_create_child(JsonNode* parentNode, const char* key, JsonNodeType type)
{
	JsonNodeInternal* result = 0;
	JsonNodeInternal* parent = (JsonNodeInternal*)parentNode;

	if ( parent->type != JsonNodeType_Object ) {
		log_err("Trying to create child on node that is not an object node.\n");
		goto out;
	}

	MemoryArenaFixed* arena = parent->arena;
	result = MAFAllocStruct(arena, JsonNodeInternal, "Json Child Node");
	if ( !result ) {
		log_err("Json Document arena is out of memory.\n");
		goto out;
	}

	u32 keyLength = (u32)StringLength(key);
	char* keyStr = MAFAllocArray(arena, char, keyLength, "Json Child Node Key");
	if ( !keyStr ) {
		log_err("Json Document arena is out of memory.\n");
		goto out;
	}

	*result = (JsonNodeInternal)ZeroStruct;
	json_internal_add_field_node(parent, result);

	result->type = type;
	MemoryCopy(keyStr, key, keyLength);
	result->key = (str8){ .capacity = keyLength, .size = keyLength, .string = keyStr };

out:
	return result;
}

JsonNodeType json_node_get_type(JsonNode* node)
{
	return ((JsonNodeInternal*)node)->type;
}

// Json Node Values

f64 json_node_get_value_as_number(JsonNode* node)
{
	f64 result = 0;
	JsonNodeInternal* n = (JsonNodeInternal*)node;

	if ( n->type != JsonNodeType_Number ) {
		log_err("Trying to get number value on Json node of type %s.\n", json_typename(n->type));
		goto out;
	}

	result = n->value.number;

out:
	return result;
}

b8 json_node_get_value_as_boolean(JsonNode* node)
{
	b8 result = 0;
	JsonNodeInternal* n = (JsonNodeInternal*)node;

	if ( n->type != JsonNodeType_Bool ) {
		log_err("Trying to get boolean value on Json node of type %s.\n", json_typename(n->type));
		goto out;
	}

	result = n->value.boolean;

out:
	return result;
}

str8 json_node_get_value_as_string(JsonNode* node)
{
	str8 result = (str8)ZeroStruct;
	JsonNodeInternal* n = (JsonNodeInternal*)node;

	if ( n->type != JsonNodeType_String ) {
		log_err("Trying to get str8 value on Json node of type %s.\n", json_typename(n->type));
		goto out;
	}

	result = n->value.string;

out:
	return result;
}

// Json Object Nodes

JsonNode* json_node_object_get_field(JsonNode* parent, const char* key)
{
	JsonNodeInternal* result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)parent;

	if ( node->type != JsonNodeType_Object ) {
		log_err("Trying to access field \"%s\" on non-object / %s node.\n", key, json_typename(node->type));
		goto out;
	}

	for (u32 i = 0; i < node->value.object.fieldCount; i++) {
		JsonNodeInternal* field = node->value.object.fields[i];
		u32 keyLength = (u32)StringLength(key);
		b8 sizeMatch = keyLength == field->key.size;
		if ( sizeMatch && MemoryCompare(key, field->key.string, keyLength) ) {
			result = field;
			break;
		}
	}

out:
	return result;
}

u32 json_node_object_get_field_count(JsonNode* node)
{
	JsonNodeInternal* n = (JsonNodeInternal*)node;
	if ( n->type != JsonNodeType_Object ) return 0;
	return n->value.object.fieldCount;
}

JsonNode* json_node_add_field_number(JsonNode* node, const char* key, double value)
{
	JsonNodeInternal* result = (JsonNodeInternal*)json_node_create_child(node, key, JsonNodeType_Number);
	if ( !result ) {
		log_err("Error creating number property node \"%s\" with value %.3f\n", key, value);
		goto out;
	}

	result->value.number = value;

out:
	return result;
}

JsonNode* json_node_add_field_boolean(JsonNode* node, const char* key, b8 value)
{
	JsonNodeInternal* result = (JsonNodeInternal*)json_node_create_child(node, key, JsonNodeType_Bool);
	if ( !result ) {
		log_err("Error creating bool property node \"%s\" with value \"%s\"\n", key, value ? "true" : "false");
		goto out;
	}

	result->value.boolean = value;

out:
	return result;
}

JsonNode* json_node_add_field_string(JsonNode* node, const char* key, const char* value)
{
	JsonNodeInternal* result = (JsonNodeInternal*)json_node_create_child(node, key, JsonNodeType_String);
	if ( !result ) {
		log_err("Error creating string property node \"%s\" with value \"%s\"\n", key, value);
		goto out;
	}

	u32 valueStrLength = (u32)StringLength(value);
	char* valueStr = MAFAllocArray(result->arena, char, valueStrLength, "Json String Property Value");
	if ( !valueStr ) {
		log_err("Json Document arena is out of memory.\n");
		goto out;
	}

	MemoryCopy(valueStr, value, valueStrLength);
	result->value.string = (str8){ .capacity = valueStrLength, .size = valueStrLength, .string = valueStr };

out:
	return result;
}

JsonArray* json_node_add_field_array(JsonNode* node, const char* key, JsonNodeType valueType)
{
	JsonNodeInternal* result = (JsonNodeInternal*)json_node_create_child(node, key, JsonNodeType_Array);
	if ( !result ) {
		log_err("Error creating array property node \"%s\".\n", key);
		goto out;
	}

	result->value.array.valueType = valueType;
	result->value.array.entryCount = 0;

out:
	return result;
}

// Json Array Nodes

u32 json_array_get_element_count(JsonArray* array)
{
	u32 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)array;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to access array element count on Json node of type %s.\n", json_typename(node->type));
		goto out;
	}

	result = node->value.array.entryCount;
	
out:
	return result;
}

internal b8 json_internal_array_perform_necessary_checks_for_access(JsonArray* array, JsonNodeType valueTypeRequested, u32 at)
{
	b8 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)array;
	
	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to access array member on Json node of type %s.\n", json_typename(node->type));
		goto out;
	}

	JsonNodeType valueType = node->value.array.valueType;
	if ( valueType != valueTypeRequested ) {
		log_err("Trying to access array member of type %s when the array value type is %s.\n", json_typename(valueTypeRequested), json_typename(valueType));
		goto out;
	}

	u32 entryCount = node->value.array.entryCount;
	if ( entryCount <= at ) {
		log_err("Trying to access array member at position %d when the array has %d entries.\n", at, entryCount);
		goto out;
	}

	result = 1;

out:
	return result;
}

JsonNode* json_array_get_node_at(JsonArray* array, u32 at)
{
	JsonNode* result = 0;
	
	if ( json_internal_array_perform_necessary_checks_for_access(array, JsonNodeType_Object, at) ) {
		JsonNodeInternal* node = (JsonNodeInternal*)array;
		result = node->value.array.entries[at];
	}

	return result;
}

f64 json_array_get_number_at(JsonArray* array, u32 at)
{
	f64 result = 0;
	
	if ( json_internal_array_perform_necessary_checks_for_access(array, JsonNodeType_Number, at) ) {
		JsonNodeInternal* node = (JsonNodeInternal*)array;
		result = node->value.array.entries[at]->value.number;
	}

	return result;
}

b8 json_array_get_boolean_at(JsonArray* array, u32 at)
{
	b8 result = 0;

	if ( json_internal_array_perform_necessary_checks_for_access(array, JsonNodeType_Bool, at) ) {
		JsonNodeInternal* node = (JsonNodeInternal*)array;
		result = node->value.array.entries[at]->value.boolean;
	}

	return result;
}

str8 json_array_get_string_at(JsonArray* array, u32 at)
{
	str8 result = (str8)ZeroStruct;

	if ( json_internal_array_perform_necessary_checks_for_access(array, JsonNodeType_String, at) ) {
		JsonNodeInternal* node = (JsonNodeInternal*)array;
		result = node->value.array.entries[at]->value.string;
	}

	return result;
}

void json_internal_array_add_member_node(JsonNodeInternal* arrayNode, JsonNodeInternal* memberNode)
{
	json_internal_set_parent_child_relationship(arrayNode, memberNode);

	//- @TODO(abe) : Check that there is no overflow here

	// Sets the value of the previous field node's next pointer
	if ( arrayNode->value.array.entryCount ) {
		arrayNode->value.array.entries[arrayNode->value.array.entryCount - 1]->next = memberNode;
	}

	arrayNode->value.array.entries[arrayNode->value.array.entryCount++] = memberNode;
	memberNode->type = arrayNode->value.array.valueType;

	return;
}

internal JsonNodeInternal* json_internal_array_create_and_append_node(JsonNodeInternal* arrayNode)
{
	JsonNodeInternal* result = 0;

	MemoryArenaFixed* arena = arrayNode->arena;
	result = MAFAllocStruct(arena, JsonNodeInternal, "Json Array Member Node");
	if ( !result ) {
		log_err("Json Document arena is out of memory.\n");
		goto out;
	}

	*result = (JsonNodeInternal)ZeroStruct;
	json_internal_array_add_member_node(arrayNode, result);

out:
	return result;
}

b8 json_array_append_number(JsonArray* arrayNode, double value)
{
	b8 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)arrayNode;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to append array member to non array json node \"%.*s\".\n", node->key.size, node->key.string);
		goto out;
	}

	JsonNodeType arrayValueType = node->value.array.valueType;
	if ( arrayValueType != JsonNodeType_Number ) {
		log_err("Trying to append number to json array node of type %s.\n", json_typename(arrayValueType));
		goto out;
	}

	JsonNodeInternal* numberNode = json_internal_array_create_and_append_node(node);
	if ( !numberNode ) {
		log_err("Failed to append number to json array \"%.*s\"\n", node->key.size, node->key.string);
		goto out;
	}

	numberNode->value.number = value;

out:
	return result;
}

b8 json_array_append_boolean(JsonArray* arrayNode, b8 value)
{
	b8 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)arrayNode;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to append array member to non array json node \"%.*s\".\n", node->key.size, node->key.string);
		goto out;
	}

	JsonNodeType arrayValueType = node->value.array.valueType;
	if ( arrayValueType != JsonNodeType_Bool ) {
		log_err("Trying to append boolean to json array node of type %s.\n", json_typename(arrayValueType));
		goto out;
	}

	JsonNodeInternal* booleanNode = json_internal_array_create_and_append_node(node);
	if ( !booleanNode ) {
		log_err("Failed to append boolean to json array \"%.*s\"\n", node->key.size, node->key.string);
		goto out;
	}

	booleanNode->value.boolean = value;

out:
	return result;
}

b8 json_array_append_str8(JsonArray* arrayNode, str8 value)
{
	b8 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)arrayNode;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to append array member to non array json node \"%.*s\".\n", node->key.size, node->key.string);
		goto out;
	}

	JsonNodeType arrayValueType = node->value.array.valueType;
	if ( arrayValueType != JsonNodeType_String ) {
		log_err("Trying to append string to json array node of type %s.\n", json_typename(arrayValueType));
		goto out;
	}

	JsonNodeInternal* stringNode = json_internal_array_create_and_append_node(node);
	if ( !stringNode ) {
		log_err("Failed to append string to json array \"%.*s\"\n", node->key.size, node->key.string);
		goto out;
	}

	stringNode->value.string = value;

out:
	return result;
}

b8 json_array_append_string(JsonArray* arrayNode, const char* value)
{
	b8 result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)arrayNode;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to append array member to non array json node \"%.*s\".\n", node->key.size, node->key.string);
		goto out;
	}

	JsonNodeType arrayValueType = node->value.array.valueType;
	if ( arrayValueType != JsonNodeType_String ) {
		log_err("Trying to append string to json array node of type %s.\n", json_typename(arrayValueType));
		goto out;
	}

	JsonNodeInternal* stringNode = json_internal_array_create_and_append_node(node);
	if ( !stringNode ) {
		log_err("Failed to append string to json array \"%.*s\"\n", node->key.size, node->key.string);
		goto out;
	}

	u32 strLength = StringLength(value);
	char* str = MAFAllocArray(stringNode->arena, char, StringLength(value), "Json Node Array String Value");
	MemoryCopy(str, value, strLength);
	stringNode->value.string = (str8){ .capacity = strLength, .size = strLength, .string = str };

out:
	return result;
}

JsonNode* json_array_append_object(JsonArray* arrayNode)
{
	JsonNode* result = 0;
	JsonNodeInternal* node = (JsonNodeInternal*)arrayNode;

	if ( node->type != JsonNodeType_Array ) {
		log_err("Trying to append array member to non array json node \"%.*s\".\n", node->key.size, node->key.string);
		goto out;
	}

	JsonNodeType arrayValueType = node->value.array.valueType;
	if ( arrayValueType != JsonNodeType_Object ) {
		log_err("Trying to append object to json array node of type %s.\n", json_typename(arrayValueType));
		goto out;
	}

	result = json_internal_array_create_and_append_node(node);
	if ( !result ) {
		log_err("Failed to append object to json array \"%.*s\"\n", node->key.size, node->key.string);
		goto out;
	}

out:
	return result;
}

//- Others

void JsonPrintNode(JsonNode* nodeToPrint, i32 level)
{
	JsonNodeInternal* node = (JsonNodeInternal*)nodeToPrint;

	switch (node->type) {
		case JsonNodeType_Object:
		{
			log_msg("Starting object \"%.*s\": %d field(s)\n",
				node->key.size,
				node->key.string,
				node->value.object.fieldCount
			);

			for (u32 i = 0; i < node->value.object.fieldCount; i++) {
				JsonPrintNode(*(node->value.object.fields + i), level + 1);
			}
		} break;

		case JsonNodeType_Array:
		{
			log_msg("Starting array \"%.*s\": %d entries\n",
				node->key.size,
				node->key.string,
				node->value.array.entryCount
			);

			for (u32 i = 0; i < node->value.array.entryCount; i++) {
				JsonPrintNode(*(node->value.array.entries + i), level + 1);
			}
		} break;

		case JsonNodeType_Number:
		{
			log_msg("\"%.*s\" = %.3f\n", node->key.size, node->key.string, node->value.number);
		} break;

		case JsonNodeType_String:
		{
			log_msg("\"%.*s\" = \"%.*s\"\n", 
				node->key.size,
				node->key.string,
				node->value.string.size,
				node->value.string.string
			);
		} break;

		case JsonNodeType_Bool:
		{
			log_msg("\"%.*s\" = %s\n", node->key.size, node->key.string, node->value.boolean ? "true" : "false");
		} break;

		case JsonNodeType_Null:
		{
			log_msg("\"%.*s\" = null\n", node->key.size, node->key.string);
		} break;

		default:
		{
			log_msg("Unsupported node type %d for \"%.*s\".\n",
				node->type,
				node->key.size,
				node->key.string
			);
		} break;
	}
}

b8 json_node_write_to_stream_recursive(Stream* stream, JsonNodeInternal* node, b8 pretty, i32 level)
{
	if ( pretty ) {
		for (i32 i = 0; i < level; i++) stream_write_str8(stream, MakeStr8("\t"));
	}

	//- Only writes node key if :
	//  * the current node has a parent node (it is not the root node) ;
	//  * the current node is not an array member node (parent is not an array).

	if ( node->parent && node->parent->type != JsonNodeType_Array ) {
		stream_write_str8(stream, MakeStr8("\""));
		stream_write_str8(stream, node->key);
		stream_write_str8(stream, MakeStr8("\": "));
	}

	switch ( node->type ) {
		case JsonNodeType_Object:
		{
			stream_write_str8(stream, MakeStr8("{"));
			if ( pretty ) {
				stream_write_str8(stream, MakeStr8("\n"));
			}

			for (u32 i = 0; i < node->value.object.fieldCount; i++) {
				json_node_write_to_stream_recursive(stream, *(node->value.object.fields + i), pretty, level + 1);
			}

			if ( pretty ) {
				for (i32 i = 0; i < level; i++) stream_write_str8(stream, MakeStr8("\t"));
			}

			stream_write_str8(stream, MakeStr8("}"));
		} break;

		case JsonNodeType_Array:
		{
			stream_write_str8(stream, MakeStr8("["));
			if ( pretty ) {
				stream_write_str8(stream, MakeStr8("\n"));
			}

			for (u32 i = 0; i < node->value.array.entryCount; i++) {
				json_node_write_to_stream_recursive(stream, *(node->value.array.entries + i), pretty, level + 1);
			}

			if ( pretty ) {
				for (i32 i = 0; i < level; i++) stream_write_str8(stream, MakeStr8("\t"));
			}

			stream_write_str8(stream, MakeStr8("]"));
		} break;

		case JsonNodeType_Number:
		{
			char buffer[32] = "";
			snprintf(buffer, sizeof(buffer), "%.3f", node->value.number);
			stream_write_str8(stream, MakeStr8(buffer));
		} break;

		case JsonNodeType_String:
		{
			stream_write_str8(stream, MakeStr8("\""));
			stream_write_str8(stream, node->value.string);
			stream_write_str8(stream, MakeStr8("\""));
		} break;

		case JsonNodeType_Bool:
		{
			stream_write_str8(stream, MakeStr8(node->value.boolean ? "true" : "false"));
		} break;

		case JsonNodeType_Null:
		{
			stream_write_str8(stream, MakeStr8("null"));
		} break;

		default:
		{
			log_msg("Unsupported node type %d for \"%.*s\".\n",
				node->type,
				node->key.size,
				node->key.string
			);
		} break;
	}

	//- Only writes a comma if :
	//  * the current node has a parent node (it is not the root node) ;
	//  * the current node has one node after it in the field list (it is node the last node).

	if ( node->parent && node->next ) {
		stream_write_str8(stream, MakeStr8(","));
	}

	if ( pretty ) {
		stream_write_str8(stream, MakeStr8("\n"));
	}

	return 1;
}
