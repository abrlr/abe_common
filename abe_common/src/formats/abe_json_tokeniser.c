#include "abe_common/include/abe_log.h"

#include "abe_json_tokeniser.h"

//- NOTE(abe): @TODO
//
//  There is an incorrect handling of arrays of objects
//  Currently, an array of objects with 2 fields each is parsed as having 4 entries
//  Instead we should have 2 entries with 2 fields each
//

typedef u32 JsonTokenType;
typedef enum JsonTokenType_ {
	JsonTokenType_Invalid,
	JsonTokenType_Identifier,
	JsonTokenType_Number,
	JsonTokenType_Colon,
	JsonTokenType_Comma,
	JsonTokenType_BracketOpen,
	JsonTokenType_BracketClose,
	JsonTokenType_SquareBracketOpen,
	JsonTokenType_SquareBracketClose,
	JsonTokenType_Quote,
	JsonTokenType_Count
} JsonTokenType_;

typedef struct JsonToken JsonToken;
struct JsonToken {
	JsonToken* previous;
	JsonToken* next;
	JsonTokenType type;
	str8 token;
	u32 line;
};

global const char* g_JsonTokenTypeToString[] = {
	"Invalid",
	"Identifier",
	"Number",
	"Colon",
	"Comma",
	"BracketOpen",
	"BracketClose",
	"SquareBracketOpen",
	"SquareBracketClose",
	"Quote",
};

s_assert(ArrayCount(g_JsonTokenTypeToString) == JsonTokenType_Count, "Missing Token String");

internal char* EatWhitespaces(char* cursor, u32* lineCount)
{
	while ( IsWhitespace(*cursor) ) {
		if ( *cursor == '\n' ) (*lineCount)++;
		cursor++;
	}

	return cursor;
}

inline internal b8 IsValidJsonStringCharacter(char c)
{
	return (
		IsAlpha(c) ||
		IsNumber(c) ||
		IsSpecial(c) ||
		IsParenVar(c) ||
		IsOperator(c) ||
		IsWhitespace(c) ||
		c == '\''
	);
}

internal JsonToken* JsonTokeniseStr8(MemoryArenaFixed* arena, str8 file)
{
	char* cursor = file.string;
	i32 line = 0;

	JsonToken* start = MAFAllocStruct(arena, JsonToken, "json_first_token");
	JsonToken* current = start;

	b8 currentlyParsingAString = 0;

	while ( cursor && cursor < file.string + file.size ) {
		cursor = EatWhitespaces(cursor, &line);
		current->token.string = cursor;
		current->line = line;

		switch ( *cursor ) {
			case ':':
			{
				current->token.size = 1;
				current->type = JsonTokenType_Colon;
				cursor++;
			} break;

			case ',':
			{
				current->token.size = 1;
				current->type = JsonTokenType_Comma;
				cursor++;
			} break;

			case '{':
			{
				current->token.size = 1;
				current->type = JsonTokenType_BracketOpen;
				cursor++;
			} break;

			case '}':
			{
				current->token.size = 1;
				current->type = JsonTokenType_BracketClose;
				cursor++;
			} break;

			case '[':
			{
				current->token.size = 1;
				current->type = JsonTokenType_SquareBracketOpen;
				cursor++;
			} break;

			case ']':
			{
				current->token.size = 1;
				current->type = JsonTokenType_SquareBracketClose;
				cursor++;
			} break;

			case '"':
			{
				if ( currentlyParsingAString ) {
					currentlyParsingAString = 0;
				} else if ( !currentlyParsingAString ) {
					currentlyParsingAString = 1;
				}

				current->token.size = 1;
				current->type = JsonTokenType_Quote;
				cursor++;
			} break;

			default:
			{
				if ( currentlyParsingAString ) {
					if ( *cursor == '\\' && *(cursor+1) == '"' ) {
						cursor += 2;
					}

					while ( IsValidJsonStringCharacter(*cursor) ) {
						cursor++;

						if ( *cursor == '\\' && *(cursor+1) == '"' ) {
							cursor += 2;
						}
					}

					current->type = JsonTokenType_Identifier;
				}

				else {
					if ( IsAlpha(*cursor) ) {
						while ( IsAlpha(*cursor) ) {
							cursor++;
						}

						current->type = JsonTokenType_Identifier;
					} else if ( IsNumber(*cursor) ) {
						while ( IsNumber(*cursor) || *cursor == '.' ) {
							cursor++;
						}

						current->type = JsonTokenType_Number;
					} else {
						cursor++;
						current->type = JsonTokenType_Invalid;
					}
				}

				current->token.size = (u32)(cursor - current->token.string);
			} break;
		}

		JsonToken* previous = current;
		current = MAFAllocStruct(arena, JsonToken, "json_token");
		*current = (JsonToken)ZeroStruct;
		current->previous = previous;
		previous->next = current;

#if ABE_JSON_DEBUG_PRINT
		log_msg("Token %s: \"%.*s\".\n",
			g_JsonTokenTypeToString[previous->type],
			previous->token.size,
			previous->token.string
		);
#endif
	}

	return start;
}

inline internal b8 ExpectToken(JsonToken* token, JsonTokenType type)
{
	if ( token->type != type ) {
		return 0;
	}

	return 1;
}

internal b8 JsonParseObject(MemoryArenaFixed* arena, JsonToken** token, JsonNode* object);
internal b8 JsonParseArray(MemoryArenaFixed* arena, JsonToken** token, JsonNode* array);

internal b8 JsonParseIdentifier(MemoryArenaFixed* arena, JsonToken** token, JsonNode* parentNode)
{
	b8 result = 0;
	JsonNodeInternal* parent = (JsonNodeInternal*)parentNode;

	//- Validate key format
	JsonToken* identifier = (*token)->next;
	if ( !ExpectToken(identifier, JsonTokenType_Identifier) ) {
		log_err("Line %d : Expected identifier after opening quote but found %s.\n",
			identifier->line,
			g_JsonTokenTypeToString[identifier->type]
		);
		
		goto out;
	}

	JsonToken* closeQuote = identifier->next;
	if ( !ExpectToken(closeQuote, JsonTokenType_Quote) ) {
		log_err("Line %d : Unexpected %s after identifier \"%.*s\".\n",
			closeQuote->line,
			g_JsonTokenTypeToString[closeQuote->type],
			identifier->token.size,
			identifier->token.string
		);
		
		goto out;
	}

	JsonToken* colon = closeQuote->next;
	if ( !ExpectToken(colon, JsonTokenType_Colon) ) {
		log_err("Line %d : Unexpected %s after closing quote.\n",
			colon->line,
			g_JsonTokenTypeToString[colon->type]
		);
	
		goto out;
	}

	JsonNodeInternal* node = MAFAllocStruct(arena, JsonNodeInternal, "json_node");
	*node = (JsonNodeInternal)ZeroStruct;
	node->key = identifier->token;

	if ( parent ) {
		switch (parent->type) {
			case JsonNodeType_Object: json_internal_add_field_node(parent, node); break;
			case JsonNodeType_Array: json_internal_array_add_member_node(parent, node); break;
		}
	}

	JsonToken* next = colon->next;
	switch ( next->type ) {
		case JsonTokenType_BracketOpen:
		{
#if ABE_JSON_DEBUG_PRINT
			log_info("Line %d : Detected identifier \"%.*s\" to be an object.\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string
			);
#endif

			node->type = JsonNodeType_Object;
			*token = next;

			JsonParseObject(arena, token, node);

			*token = (*token)->next; // Skip closing bracket
		} break;

		case JsonTokenType_SquareBracketOpen:
		{
#if ABE_JSON_DEBUG_PRINT
			log_info("Line %d : Detected identifier \"%.*s\" to be an array.\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string
			);
#endif

			node->type = JsonNodeType_Array;
			*token = next;

			JsonParseArray(arena, token, node);

			*token = (*token)->next; // Skip closing square bracket

			if ( node->value.array.entryCount ) {
				node->value.array.valueType = node->value.array.entries[0]->type;
			}
		} break;

		case JsonTokenType_Quote:
		{
			JsonToken* stringStart = next->next;
			JsonToken* stringEnd = next->next;

			while ( stringEnd->type != JsonTokenType_Quote ) {
				stringEnd = stringEnd->next;
			}
			
			u32 stringLength = (u32)(stringEnd->token.string + stringEnd->token.size - stringStart->token.string - 1);

#if ABE_JSON_DEBUG_PRINT
			log_info("Line %d : Detected identifier \"%.*s\" to be a string: \"%.*s\".\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string,
				stringLength,
				stringStart->token.string
			);
#endif

			*token = stringEnd->next;

			node->type = JsonNodeType_String;
			node->value.string = (str8){
				.size = stringLength,
				.string = stringStart->token.string,
			};
		} break;

		case JsonTokenType_Number:
		{
			JsonToken* number = next;
			f32 numberRead = ReadF32(number->token);

#if ABE_JSON_DEBUG_PRINT
			log_info("Line %d : Detected identifier \"%.*s\" to be a number: %.3f.\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string,
				numberRead
			);
#endif

			*token = next->next;

			node->type = JsonNodeType_Number;
			node->value.number = numberRead;
		} break;

		case JsonTokenType_Identifier:
		{
			JsonToken* identifierValue = next;

#if ABE_JSON_DEBUG_PRINT
			log_info("Line %d : Detected identifier \"%.*s\" to be bool or null.\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string
			);
#endif
			
			b8 identifierIsFalse = CompareStrTo(identifierValue->token, "false");
			b8 identifierIsTrue = CompareStrTo(identifierValue->token, "true");
			b8 identifierIsNull = CompareStrTo(identifierValue->token, "null");

			if ( identifierIsFalse || identifierIsTrue ) {
				node->type = JsonNodeType_Bool;
				node->value.boolean = identifierIsTrue ? 1 : 0;
			} else if ( identifierIsNull ) {
				node->type = JsonNodeType_Null;
			}

			*token = next->next;
		} break;

		default:
		{
			log_err("Unexpected error at line %d: \"%.*s\".\n",
				identifier->line,
				identifier->token.size,
				identifier->token.string
			);

			goto out;
		} break;
	}

	result = 1;

out:
	return result;
}

internal b8 JsonParseObject(MemoryArenaFixed* arena, JsonToken** token, JsonNode* object)
{
	b8 result = 0;

	JsonToken* openBracket = *token;
	if ( !ExpectToken(openBracket, JsonTokenType_BracketOpen) ) {
		log_err("Expected \"{\" but got %s instead.\n", g_JsonTokenTypeToString[openBracket->type]);
		goto out;
	}

	JsonToken* t = openBracket->next;
	while ( t->type != JsonTokenType_BracketClose ) {

		JsonToken* current = t;
		if ( !JsonParseIdentifier(arena, &t, object) ) {
			log_err("Failed to parse identifier of type %s: \"%.*s\".\n",
				g_JsonTokenTypeToString[current->type],
				current->token.size,
				current->token.string
			);

			goto out;
		}

		if ( t->type != JsonTokenType_BracketClose ) {
			t = t->next;
		}
	}

	*token = t;
	result = 1;

out:
	return result;
}

internal b8 JsonParseArrayMember(MemoryArenaFixed* arena, JsonToken** token, JsonNode* arrayNodeTarget, u32 index)
{
	b8 result = 0;

	JsonNodeInternal* arrayNode = (JsonNodeInternal*)arrayNodeTarget;
	JsonNodeInternal* memberNode = MAFAllocStruct(arena, JsonNodeInternal, "json_array_member");
	*memberNode = (JsonNodeInternal)ZeroStruct;
	json_internal_array_add_member_node(arrayNode, memberNode);

	JsonToken* t = *token;

	switch (t->type) {
		case JsonTokenType_Quote:
		{
			memberNode->type = JsonNodeType_String;

			JsonToken* memberStringToken = t->next;
			memberNode->value.string = memberStringToken->token;

			JsonToken* closingQuoteToken = memberStringToken->next;
			JsonToken* commaToken = closingQuoteToken->next;
			*token = commaToken;
		} break;

		case JsonTokenType_Number:
		{
			memberNode->type = JsonNodeType_Number;

			JsonToken* numberToken = t;
			memberNode->value.number = atof(numberToken->token.string);

			JsonToken* commaToken = numberToken->next;
			*token = commaToken;
		} break;

		case JsonTokenType_BracketOpen:
		{
			memberNode->type = JsonNodeType_Object;
			JsonParseObject(arena, token, memberNode);
			*token = (*token)->next;
		} break;

		case JsonTokenType_Identifier:
		{
			//memberNode->type = JsonNodeType_Bool;

			b8 identifierIsFalse = CompareStrTo((*token)->token, "false");
			b8 identifierIsTrue = CompareStrTo((*token)->token, "true");
			b8 identifierIsNull = CompareStrTo((*token)->token, "null");

			if (identifierIsFalse || identifierIsTrue) {
				memberNode->type = JsonNodeType_Bool;
				memberNode->value.boolean = identifierIsTrue ? 1 : 0;
			}
			else if (identifierIsNull) {
				memberNode->type = JsonNodeType_Null;
			}

			*token = t->next;
		} break;

		default:
		{
			log_err("Token \"%.*s\" is not a valid token for an array entry.\n", t->token.size, t->token.string);
		} break;
	}

	result = 1;

	return result;
}

internal b8 JsonParseArray(MemoryArenaFixed* arena, JsonToken** token, JsonNode* array)
{
	b8 result = 0;

	JsonToken* openSquareBracket = *token;
	if ( !ExpectToken(openSquareBracket, JsonTokenType_SquareBracketOpen) ) {
		log_err("Expected \"[\" bug got %s instead.\n", g_JsonTokenTypeToString[openSquareBracket->type]);
		goto out;
	}

	u32 arrayIndex = 0;
	JsonToken* t = openSquareBracket->next;
	while ( t->type != JsonTokenType_SquareBracketClose ) {

		JsonToken* current = t;
		if ( !JsonParseArrayMember(arena, &t, array, arrayIndex++) ) {
			log_err("Failed to parse identifier of type %s: \"%.*s\"\n",
				g_JsonTokenTypeToString[current->type],
				current->token.size,
				current->token.string
			);

			goto out;
		}

		if ( t->type != JsonTokenType_SquareBracketClose ) {
			t = t->next;
		}

#if ABE_JSON_DEBUG_PRINT
		log_msg("Line %d : Next token to be parsed will be of type %s: \"%.*s\".\n",
			t->line,
			g_JsonTokenTypeToString[t->type],
			t->token.size,
			t->token.string
		);
#endif

	}
	
	*token = t;
	result = 1;

out:
	return result;
}

internal JsonNode* JsonInterpretTokenList(MemoryArenaFixed* arena, JsonToken* start)
{
	JsonNode* result = 0;

	JsonNodeInternal* root = MAFAllocStruct(arena, JsonNodeInternal, "json_root");
	*root = (JsonNodeInternal)ZeroStruct;
	root->type = JsonNodeType_Object;
	root->arena = arena;

	JsonToken* currentToken = start;
	if ( !ExpectToken(currentToken, JsonTokenType_BracketOpen) ) {
		log_err("Line %d : Unexpected token of type %s: \"%.*s\".\n",
				currentToken->line,
				g_JsonTokenTypeToString[currentToken->type], 
				currentToken->token.size,
				currentToken->token.string
		);
		goto out;
	}

	currentToken = currentToken->next;
	
	for (JsonToken* token = currentToken; token; token = token->next) {
		switch ( token->type ) {
			case JsonTokenType_Quote:
			{
				JsonParseIdentifier(arena, &token, root);
			} break;
		}
	}

	result = root;

out:
	return result;
}

JsonNode* JsonParseStr8(MemoryArenaFixed* arena, str8 str)
{
	JsonNode* result = 0;

	JsonToken* start = JsonTokeniseStr8(arena, str);
	if ( !start ) {
		log_err("Failed to tokenise JSON str8.\n");
		goto out;
	}

	result = JsonInterpretTokenList(arena, start);
	if ( !result ) {
		log_err("Failed to interpret JSON token list.\n");
		goto out;
	}

out:
	return result;
}

JsonNodeInternal* json_tokeniser_generate_document_from_stream(MemoryArenaFixed* arena, Stream* stream)
{
	JsonNodeInternal* result = 0;
	assert(result);
	return result;
}