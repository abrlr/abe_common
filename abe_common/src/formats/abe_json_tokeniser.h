#ifndef AbeFormatsJsonTokeniser_h
#define AbeFormatsJsonTokeniser_h

#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_stream.h"

#include "abe_json_internal.h"

JsonNodeInternal* json_tokeniser_generate_document_from_stream(MemoryArenaFixed* arena, Stream* stream);

#endif // AbeFormatsJsonTokeniser_h
