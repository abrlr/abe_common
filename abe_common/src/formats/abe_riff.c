#include "abe_common/include/formats/abe_riff.h"

u32 riff_file_iterator_get_chunk_type(riff_file_iterator it)
{
    riff_chunk_header* chunk = (riff_chunk_header*)it.at;
    return chunk->id;
}

riff_file_iterator riff_file_iterator_new(void* content, u32 size)
{
    assert(content);
    riff_file_iterator result = ZeroStruct;
    result.at = content;
    result.end = (u8*)content + size;
    
    return result;
}

b8 riff_file_iterator_valid(riff_file_iterator* it)
{
    return it->at < it->end;
}

void riff_file_iterate(riff_file_iterator* it)
{
    riff_chunk_header* chunk = (riff_chunk_header*)it->at;
    u32 size = (chunk->size + 1) & ~1;
    it->at = (u8*)it->at + sizeof(riff_chunk_header) + size;
}
