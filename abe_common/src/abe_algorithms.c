#include "abe_common/include/abe_algorithms.h"

// i32 arrays
internal void algorithms_i32_swap(i32* a, i32* b)
{
    i32 t = *a;
    *a = *b;
    *b = t;
}

internal i32 algorithms_i32_quicksort_partition(i32* arr, i32 low, i32 high)
{
    i32 pivot = arr[high];
    i32 i = (low - 1);
    
    for (i32 j = low; j <= high; j++) {
        if ( arr[j] < pivot ) {
            i++;
            algorithms_i32_swap(arr + i, arr + j);
        }
    }
    
    algorithms_i32_swap(arr + i + 1, arr + high);
    return i + 1;
}

void algorithms_i32_quicksort(i32* arr, i32 low, i32 high)
{
    if ( low < high ) {
        i32 partitionIndex = algorithms_i32_quicksort_partition(arr, low, high);
        
        algorithms_i32_quicksort(arr, low, partitionIndex - 1);
        algorithms_i32_quicksort(arr, partitionIndex + 1, high);
    }
}


// generic arrays using f32 keys
internal void algorithms_swap(void* a, void* b, u32 size)
{
    assert(size < 256);
    char buffer[256];
    MemoryCopy(buffer, a, size);
    MemoryCopy(a, b, size);
    MemoryCopy(b, buffer, size);
}

internal i32 algorithms_quicksort_partition(f32* keys, void* values, u32 valueSize, i32 low, i32 high)
{
    f32 pivot = keys[high];
    i32 i = (low - 1);
    
    for (i32 j = low; j <= high; j++) {
        if ( keys[j] < pivot ) {
            i++;
            algorithms_swap(keys + i, keys + j, valueSize);
            algorithms_swap((u8*)(values) + i * valueSize, (u8*)(values) + j * valueSize, valueSize);
        }
    }
    
    algorithms_swap(keys + i + 1, keys + high, valueSize);
    algorithms_swap((u8*)(values) + ( i + 1 ) * valueSize, (u8*)(values) + high * valueSize, valueSize);
    return i + 1;
}

void algorithms_quicksort(f32* keys, void* values, u32 valueSize, i32 low, i32 high)
{
    if ( low < high ) {
        i32 partitionIndex = algorithms_quicksort_partition(keys, values, valueSize, low, high);
        
        algorithms_quicksort(keys, values, valueSize, low, partitionIndex - 1);
        algorithms_quicksort(keys, values, valueSize, partitionIndex + 1, high);
    }
}