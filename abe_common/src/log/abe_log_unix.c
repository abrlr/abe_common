#ifndef ABE_WIN
#ifndef ABE_LOG_CONSOLE_ENABLE

#include <stdio.h>
#include <stdarg.h>

#include "abe_common/include/abe_log.h"
#include "abe_common/include/abe_string.h"

// Text Reset
#define abe_log_RCol     "\e[0m"

// Blacks
#define abe_log_Bla      "\e[0;30m" 
#define abe_log_BBla     "\e[1;30m" 
#define abe_log_UBla     "\e[4;30m" 
#define abe_log_IBla     "\e[0;90m" 
#define abe_log_BIBla    "\e[1;90m" 
#define abe_log_On_Bla   "\e[40m" 
#define abe_log_On_IBla  "\e[0;100m"

// Reds
#define abe_log_Red      "\e[0;31m"
#define abe_log_BRed     "\e[1;31m"
#define abe_log_URed     "\e[4;31m"
#define abe_log_IRed     "\e[0;91m"
#define abe_log_BIRed    "\e[1;91m"
#define abe_log_On_Red   "\e[41m"
#define abe_log_On_IRed  "\e[0;101m"

// Greens
#define abe_log_Gre      "\e[0;32m"
#define abe_log_BGre     "\e[1;32m"
#define abe_log_UGre     "\e[4;32m"
#define abe_log_IGre     "\e[0;92m"
#define abe_log_BIGre    "\e[1;92m"
#define abe_log_On_Gre   "\e[42m"
#define abe_log_On_IGre  "\e[0;102m"

// Yellows
#define abe_log_Yel      "\e[0;33m"
#define abe_log_BYel     "\e[1;33m"
#define abe_log_UYel     "\e[4;33m"
#define abe_log_IYel     "\e[0;93m"
#define abe_log_BIYel    "\e[1;93m"
#define abe_log_On_Yel   "\e[43m"
#define abe_log_On_IYel  "\e[0;103m"

// Blues
#define abe_log_Blu      "\e[0;34m" 
#define abe_log_BBlu     "\e[1;34m" 
#define abe_log_UBlu     "\e[4;34m" 
#define abe_log_IBlu     "\e[0;94m" 
#define abe_log_BIBlu    "\e[1;94m" 
#define abe_log_On_Blu   "\e[44m"
#define abe_log_On_IBlu  "\e[0;104m"

/*
Pur='\e[0;35m';     BPur='\e[1;35m';    UPur='\e[4;35m';    IPur='\e[0;95m';    BIPur='\e[1;95m';   On_Pur='\e[45m';    On_IPur='\e[0;105m';
Cya='\e[0;36m';     BCya='\e[1;36m';    UCya='\e[4;36m';    ICya='\e[0;96m';    BICya='\e[1;96m';   On_Cya='\e[46m';    On_ICya='\e[0;106m';
Whi='\e[0;37m';     BWhi='\e[1;37m';    UWhi='\e[4;37m';    IWhi='\e[0;97m';    BIWhi='\e[1;97m';   On_Whi='\e[47m';    On_IWhi='\e[0;107m';
*/

void abe_log(LogLevel level, char* file, u32 line, char* format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    
    str8 fileStr = MakeStr8(file);
    char* lastSlashInFileStr = GetLastSlash(fileStr);
    
    switch ( level ) {
        case LogLevel_Msg:
        {
            printf("[Message] %s (%d): ", lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Info:
        {
            printf(abe_log_Gre "[Info] %s (%d): " abe_log_RCol, lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Warn:
        {
            printf(abe_log_Yel "[Warning] %s (%d): " abe_log_RCol, lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Err:
        {
            printf(abe_log_Red "[Error] %s (%d): " abe_log_RCol, lastSlashInFileStr, line);
        } break;
        
        default: break;
    }
    
    vfprintf(stdout, format, argptr);
    va_end(argptr);
}

#endif // not ABE_CONSOLE_ENABLE
#endif // not ABE_WIN
