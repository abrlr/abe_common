#include <stdio.h>
#include <stdarg.h>

#include "abe_common/include/abe_log.h"

#ifdef ABE_LOG_CONSOLE_ENABLE

void abe_log_set_console(log_console* console)
{
    _log_console_ptr = console;
}

internal void abe_log_move_buffer_up()
{
    if ( !_log_console_ptr ) { return; }
    
    for (i32 i = ABE_LOG_CONSOLE_LINE_COUNT - 2; i > -1; i--) {
        _log_console_ptr->lines[i+1].color = _log_console_ptr->lines[i].color;
        memcpy(_log_console_ptr->lines[i+1].lineStr, _log_console_ptr->lines[i].lineStr, ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT);
    }
    
    for (i32 i = 0; i < ABE_LOG_LINE_BODY_COUNT; i++) _log_console_ptr->lines[0].lineStr[i] = ' ';
    _log_console_ptr->lineCount++;
}

void abe_log(LogLevel leve, char* file, u32 line, char* format,...)
{
    if ( !_log_console_ptr ) { return; }
    
    va_list argptr;
    va_start(argptr, msg);
    
    abe_log_move_buffer_up();
    
    char* lastSlashInFileStr = (char*)file + strlen(file);
    while ( *lastSlashInFileStr != '/' && *lastSlashInFileStr != '\\' ) {
        lastSlashInFileStr--;
        if ( lastSlashInFileStr == file ) break;
    }
    
    char* headerStr = _log_console_ptr->lines[0].lineStr;
    sprintf(headerStr, "%s (%d): ", lastSlashInFileStr + 1, line);
    
    _log_console_ptr->lines[0].lineStr[strlen(headerStr)] = ' ';
    
    char* consoleLine = _log_console_ptr->lines[0].lineStr + ABE_LOG_LINE_HEADER_COUNT;
    vsnprintf(consoleLine, ABE_LOG_LINE_BODY_COUNT, msg, argptr);
    
    _log_console_ptr->lines[0].level = level;
    
    va_end(argptr);
}

#endif // ABE_LOG_CONSOLE_ENABLE