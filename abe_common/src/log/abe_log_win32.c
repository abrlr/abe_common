#ifdef ABE_WIN
#ifndef ABE_CONSOLE_ENABLE

#include <stdio.h>

#include "abe_common/include/abe_log.h"
#include "abe_common/include/abe_string.h"

global HANDLE g_console;

#define ABE_LOG_COLOR_WHITE  ( FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE )
#define ABE_LOG_COLOR_RED    ( FOREGROUND_RED )
#define ABE_LOG_COLOR_BLUE   ( FOREGROUND_BLUE )
#define ABE_LOG_COLOR_GREEN  ( FOREGROUND_GREEN )
#define ABE_LOG_COLOR_YELLOW ( ABE_LOG_COLOR_RED | ABE_LOG_COLOR_GREEN )

void abe_log(LogLevel level, char* file, u32 line, char* format, ...)
{
    if ( !g_console ) {
        g_console = GetStdHandle(STD_OUTPUT_HANDLE);
    }
    
    str8 fileStr = MakeStr8(file);
    char* lastSlashInFileStr = GetLastSlash(fileStr);

    switch ( level ) {
        case LogLevel_Msg:
        {
            printf("[MESSAGE] %s (%d): ", lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Info:
        {
            SetConsoleTextAttribute(g_console, ABE_LOG_COLOR_GREEN);
            printf("[INFO] %s (%d): ", lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Warn:
        {
            SetConsoleTextAttribute(g_console, ABE_LOG_COLOR_YELLOW);
            printf("[WARNING] %s (%d): ", lastSlashInFileStr, line);
        } break;
        
        case LogLevel_Err:
        {
            SetConsoleTextAttribute(g_console, ABE_LOG_COLOR_RED);
            printf("[ERROR] %s (%d): ", lastSlashInFileStr, line);
        } break;
        
        default:
        {
        } break;
    }
    
    SetConsoleTextAttribute(g_console, ABE_LOG_COLOR_WHITE);
    
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
}

#endif // ABE_CONSOLE_ENABLE
#endif // ABE_WIN