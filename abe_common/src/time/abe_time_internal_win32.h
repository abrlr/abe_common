#ifndef AbeTimerInternalWin32_h
#define AbeTimerInternalWin32_h

#include <winnt.h>

typedef struct TimerInternalWin32 TimerInternalWin32;
struct TimerInternalWin32
{
	LARGE_INTEGER frequency;
	LARGE_INTEGER start;
	LARGE_INTEGER stop;
};

#endif // AbeTimerInternal_h
