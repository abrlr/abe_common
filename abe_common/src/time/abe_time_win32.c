#ifdef ABE_WIN

#include "abe_common/include/abe_time.h"
#include "abe_time_internal_win32.h"

Timer* timer_create(MemoryArenaFixed* arena)
{
	TimerInternalWin32* result = MAFAllocStruct(arena, TimerInternalWin32, "TimerInternalWin32");
	*result = (TimerInternalWin32)ZeroStruct;

	QueryPerformanceFrequency(&result->frequency);

	return result;
}

void timer_start(Timer* timer)
{
	TimerInternalWin32* t = (TimerInternalWin32*)timer;
	QueryPerformanceCounter(&t->start);

	return;
}

void timer_stop(Timer* timer)
{
	TimerInternalWin32* t = (TimerInternalWin32*)timer;
	QueryPerformanceCounter(&t->stop);

	return;
}

f64 timer_get_elapsed_seconds(Timer* timer)
{
	f64 elapsedUS = timer_get_elapsed_microseconds(timer);
	return elapsedUS / 1'000'000;
}

f64 timer_get_elapsed_miliseconds(Timer* timer)
{
	f64 elapsedUS = timer_get_elapsed_microseconds(timer);
	return elapsedUS / 1'000;
}

f64 timer_get_elapsed_microseconds(Timer* timer)
{
	TimerInternalWin32* t = (TimerInternalWin32*)timer;
	LARGE_INTEGER elapsedMicroseconds;
	elapsedMicroseconds.QuadPart = t->stop.QuadPart - t->start.QuadPart;
	elapsedMicroseconds.QuadPart *= 1'000'000;
	elapsedMicroseconds.QuadPart /= t->frequency.QuadPart;

	return (f64)elapsedMicroseconds.QuadPart;
}

#endif // ABE_WIN