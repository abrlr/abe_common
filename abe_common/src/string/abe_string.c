#include "abe_string_internal.h"

//- [SECTION] Char Utilities

b8 IsNumber(char c)
{
    return ( (c >= '0') && (c <= '9') );
}

b8 IsAlpha(char c)
{
    b8 isLowerCase = (c >= 'a') && (c <= 'z');
    b8 isUpperCase = (c >= 'A') && (c <= 'Z');
    return ( isLowerCase || isUpperCase );
}

b8 IsWhitespace(char c)
{
    return ( (c == ' ') || 
            (c == '\n') ||
            (c == '\t') ||
            (c == '\r') );
}

b8 IsParenVar(char c)
{
	return (
			( c == '(' ) ||
			( c == '[' ) ||
			( c == '{' ) ||
			( c == ')' ) ||
			( c == ']' ) ||
			( c == '}')
	);
}

b8 IsOperator(char c)
{
	return (
			( c == '+' ) ||
			( c == '-' ) ||
			( c == '*' ) ||
			( c == '/' )
	);
}

b8 IsSpecial(char c)
{
	return ( 
			( c == '&' ) ||
			( c == '~' ) ||
			( c == '#' ) ||
			( c == '|' ) ||
			( c == '_' ) ||
			( c == '\\' ) ||
			( c == '^' ) ||
			( c == '@' ) ||
			( c == '=' ) ||
			( c == '$' ) ||
			( c == '�' ) ||
			( c == '%' ) ||
			( c == '!' ) ||
			( c == ',' ) ||
			( c == '?' ) ||
			( c == '.' ) ||
			( c == ':' ) ||
			( c == ';' ) ||
			( c == '�' )
	);
}

//- [SECTION] String Utilities

str8 MakeStr8(const char* string)
{
	i32 size = (i32)StringLength(string);
	str8 result = {
		.size = size,
		.string = (char*)string,
	};

	return result;
}

b8 AddStr8(str8* dest, str8 toAdd)
{
	if ( dest->size + toAdd.size > dest->capacity ) { return 0; }
	MemoryCopy(dest->string + dest->size, toAdd.string, toAdd.size);
	dest->size += toAdd.size;
	return 1;
}

b8 ConcatStr8(str8 first, str8 second, str8* dest)
{
	if ( first.size + second.size > dest->capacity ) { return 0; }

    char* destStr = dest->string;
    for(u32 i = 0; i < first.size; i++) {
        *destStr++ = *first.string++;
    }
    
    for(u32 i = 0; i < second.size; i++) {
        *destStr++ = *second.string++;
    }
    
    dest->size = (u32)(destStr - dest->string);
    *destStr++ = 0;

	return 1;
}

u32 GetLineLength(str8 line)
{
    u32 result = 0;

    for (u32 cursor = 0; cursor < line.size; cursor++) {
        if ( line.string[cursor] == '\n' ) {
            result = cursor + 1;
            break;
        }
    }

    return result;
}

u32 GetLineCount(str8 text)
{
    u32 result = 0;

    for (u32 cursor = 0; cursor < text.size; cursor++) {
        if ( text.string[cursor] == '\n' ) result++;
    }   
    
    return result;
}

char* GetNextLine(str8 text)
{
    char* result = text.string;

    for (u32 cursor = 0; cursor < text.size; cursor++) {
        if ( text.string[cursor] == '\n' ) {
            result = text.string + cursor + 1;
            break;
        }
    }
    
    return result;
}

char* GetLastSlash(str8 text)
{
    if ( text.size <= 0 ) { return 0; }
    if ( text.size == 1 ) { return 0; }
    char* result = text.string;

    // Goes from the end of the file string to its beginning to find a '/' or '\' char
    for (u32 cursor = text.size - 1; cursor > 0; cursor--) {
        if ( text.string[cursor] == '/' || text.string[cursor] == '\\' ) {
            result = text.string + cursor + 1;
            break;
        }
    }
    
    return result;
}

// NOTE(abe): this assumes that the output array of pointers is big enough
void SplitOnChar(str8 text, char token, str8* splits, u32* splitCount)
{
    u32 splitIndex = 0;
    
    splits[splitIndex].string = text.string;
    u32 previousEnd = 0;
    
    for (u32 i = 0; i < text.size; i++) {
        
        if ( *(text.string + i) == token ) {
            splits[splitIndex].size = i - previousEnd;
            
            // Skip whitespace char for length calculation
            previousEnd = i + 1;
            splitIndex += 1;
            splits[splitIndex].string = (char*)text.string + i + 1;
        }
    }
    
    splits[splitIndex].size = text.size - previousEnd;
    *splitCount = splitIndex + 1;
}

void SplitOnWhitespace(str8 text, str8* splits, u32* splitCount)
{
    if ( !text.string || text.size <= 0 ) { return; }
    
    u32 splitIndex = 0;
    
    splits[splitIndex].string = text.string;
    u32 previousEnd = 0;
    
    for (u32 i = 0; i < text.size; i++) {
        
        if ( IsWhitespace(*(text.string + i)) ) {
            splits[splitIndex].size = i - previousEnd;
            
            // Skip whitespace char for length calculation
            previousEnd = i + 1;
            splitIndex += 1;
            splits[splitIndex].string = (char*)text.string + i + 1;
        }
    }
    
    splits[splitIndex].size = text.size - previousEnd;
    *splitCount = splitIndex + 1;
}

u32 GetFirstTokenLocation(char token, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    for (char* linePtr = text.string; (u32)(linePtr - text.string) < text.size; linePtr++) {
        if ( token == *linePtr ) {
            return (u32)(linePtr - text.string);
        }
    }
    
    return U32Max;
}

u32 GetFirstSequenceLocation(const char* seq, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    u32 seqSize = (u32)StringLength(seq);
    
    for (char* linePtr = text.string; (u32)(linePtr - text.string) < text.size - seqSize + 1; linePtr++) {
        if ( memcmp(seq, linePtr, seqSize) == 0 ) {
            return (u32)(linePtr - text.string);;
        }
    }
    
    return U32Max;
}

u32 GetLastTokenLocation(char token, str8 text)
{
    if ( !text.string || text.size <= 0 ) { return U32Max; }
    
    for (char* ptr = text.string + text.size - 1; ptr != text.string; ptr--) {
        if ( token == *ptr ) {
            return (u32)(ptr - text.string);
        }
    }
    
    return 0;
}

b8 CompareStrTo(str8 a, const char* b)
{
	u32 len = (u32)StringLength(b);
	i32 maxLen = a.size > len ? a.size : len;
	return MemoryCompare(a.string, b, maxLen);
}

//- [SECTION] Number Parsing

internal u32 GetSizeOfDesiredNumberRead(str8 text, NumberReadFlags flags, u32 maxSize)
{
    assert(text.string != 0);
    
    // Initialise
    b8 foundDecimal = 0;
    u32 numberReadStringSize = 0;
    u32 startAt = 0;
    
    // Test for a negative number
    if ( (flags & NumberReadFlags_Sign) && (text.string[0] == '-') ) {
        numberReadStringSize++;
        startAt++;
    }
    
    // Loop over the characters of the text
    for (u32 charIndex = startAt; charIndex < text.size; charIndex++) {
        
        // Account for the decimal point
        if ( (flags & NumberReadFlags_Decimal) && (text.string[charIndex] == '.') ) {
            if ( !foundDecimal ) {
                foundDecimal = 1;
                charIndex++; // Skip the decimal
                numberReadStringSize++;
            }
            
            else break;
        }
        
        // Check if we are still getting numbers
        if ( IsNumber(text.string[charIndex]) ) {
            numberReadStringSize++;
        } else {
            break;
        }
        
        assert(numberReadStringSize < maxSize);
    }
    
    return numberReadStringSize;
}

u32 ReadU32(str8 text)
{
    u32 u32StringSize = GetSizeOfDesiredNumberRead(text, NumberReadFlags_None, 32);
    
    char charResult[32];
    MemoryZero(charResult, ArrayCount(charResult));
    MemoryCopy(charResult, text.string, u32StringSize);
    
    u64 u64result = strtoul(charResult, 0, 10);
    u32 result = SafeTruncateU64toU32(u64result);
    
    return result;
}

i32 ReadI32(str8 text)
{
    u32 i32StringSize = GetSizeOfDesiredNumberRead(text, NumberReadFlags_Sign, 32);
    
    char charResult[32];
    MemoryZero(charResult, ArrayCount(charResult));
    MemoryCopy(charResult, text.string, i32StringSize);
    
    i32 result = atoi(charResult);
    return result;
}

f32 ReadF32(str8 text)
{
    u32 f32StringSize = GetSizeOfDesiredNumberRead(text, NumberReadFlags_Float, 32);
    
    char charResult[32];
    MemoryZero(charResult, ArrayCount(charResult));
    MemoryCopy(charResult, text.string, f32StringSize);
    
    f64 result = atof(charResult);
    return (f32)result;
}

// TODO(abe): rewrite this using ReadI32
i32 ReadI32AfterTokenAndAdvance(char* token, str8* text)
{
    assert(token != 0);
    assert(text->string != 0);
    
    // Initialise
    u32 tokenSize = (u32)StringLength(token);
    u32 tokenLoc = GetFirstTokenLocation(*token, *text);
    if ( tokenLoc == U32Max ) return I32Max;
    u32 i32StringSize = 0;
    
    // Test for a negative number
    if ( text->string[tokenLoc + tokenSize + i32StringSize] == '-' ) {
        i32StringSize++;
    }
    
    while ( IsNumber(text->string[tokenLoc + tokenSize + i32StringSize]) && i32StringSize < 32 ) {
        i32StringSize++;
    }
    
    i32 result = I32Max;
    char charResult[32];
    MemoryCopy(charResult, (void*)&text->string[tokenLoc + tokenSize], i32StringSize);
    
    result = atoi(charResult);
    text->string += tokenLoc + tokenSize + i32StringSize;
    text->size -= tokenLoc + tokenSize + i32StringSize;
    
    return result;
}
