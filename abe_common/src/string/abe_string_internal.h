#ifndef AbeStringInternal_h
#define AbeStringInternal_h

#include "abe_common/include/abe_string.h"

typedef u32 NumberReadFlags;
enum NumberReadFlags_ {
    NumberReadFlags_None      = BIT(0),
    NumberReadFlags_Decimal   = BIT(1),
    NumberReadFlags_Sign      = BIT(2),
    NumberReadFlags_Float     = NumberReadFlags_Sign | NumberReadFlags_Decimal
};

#endif // AbeStringInternal_h