#include "abe_common/include/abe_random.h"

//- [SECTION] Basic Functionnality
random_series random_series_new(u32 seed)
{
    random_series result = ZeroStruct;
    result.value = seed;
    return result;
}

u32 random_u32(random_series* s)
{
    u32 result = s->value;
    result ^= result << 13;
    result ^= result >> 17;
    result ^= result << 5;
    s->value = result;
    return result;
}

f32 random_f32(random_series* s)
{
    u32 rdm = random_u32(s);
    return rdm / (f32)(U32Max);
}

f32 random_f32_in_range(random_series* s, f32 min, f32 max)
{
    f32 result = min + (max - min) * random_f32(s);
    return result;
}

i32 random_i32(random_series* s)
{
    f32 bias = random_f32_in_range(s, -1, 1);
    i32 result = f32_floor(bias * I32Max);
    return result;
}

i32 random_i32_in_range(random_series* s, i32 min, i32 max)
{
    i32 result = f32_floor(min + (max - min) * random_f32(s));
    return result;
}

//- [SECTION] GUIDs
guid guid_new(random_series* s)
{
    guid result = ZeroStruct;
    
    for (u32 i = 0; i < 4; i++) {
        u32 rdmU32 = random_u32(s);
        MemoryCopy( ( (u32*)(&result) + i ), &rdmU32, sizeof(u32) );
    }
    
    result.version = 0x4;
    result.variant = 0x1;
    
    return result;
}