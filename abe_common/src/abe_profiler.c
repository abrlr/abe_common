#include "abe_common/include/abe_log.h"
#include "abe_common/include/abe_profiler.h"

profiler* abe_profiler_ptr;

profiler_block* profiler_block_new()
{
    profiler_block* result = 0;
    profiler* p = abe_profiler_ptr;

    if ( p->count + 1 < ArrayCount(p->blocks) && !p->disabled ) {
        result = p->next++;
        *result = (profiler_block)ZeroStruct;
        
        if ( p->current ) {
            result->parent = p->current;
            profiler_block* child = p->current->child;
            
            if ( child ) {
                while ( child->next ) {
                    child = child->next;
                }
                
                child->next = result;
            }
            
            else {
                p->current->child = result;
            }
        }
        
        p->current = result;
        p->count++;
    }
    
    return result;
}

void profiler_set(profiler* p)
{
    abe_profiler_ptr = p;
    p->next = p->blocks;
    p->count = 0;
    p->current = 0;
    p->disabled = 0;
}

void profiler_begin_frame()
{
    profiler* p = abe_profiler_ptr;
    p->next = p->blocks;
    p->count = 0;
    p->current = 0;
    p->disabled = 0;
}

void profiler_end_frame()
{
    profiler* p = abe_profiler_ptr;
    p->disabled = 1;
}

void profiler_dump()
{
    profiler* p = abe_profiler_ptr;

    log_msg("Dumping %d blocks\n", p->count);
    for (u32 i = 0; i < p->count; i++) {
        profiler_block* b = p->blocks + i;
        log_msg("Block { %s (%s:%d) } took: %.6f ms\n", b->name, b->file, b->line, b->time / 1e6f);
    }
}

profiler_counter profiled_block_start(char* name, char* file, int line) 
{
    profiler* p = abe_profiler_ptr;
    profiler_counter counter = (profiler_counter)ZeroStruct;

    if ( !p->disabled ) {
        counter.block = profiler_block_new();

        if ( counter.block ) {
            if ( counter.block->parent ) {
                counter.block->depth = counter.block->parent->depth + 1;
            }
            
            MemoryCopy(counter.block->name, name, ArrayCount(counter.block->name));
            MemoryCopy(counter.block->file, file, ArrayCount(counter.block->file));
            counter.block->line = line;
            
#ifdef ABE_WIN
            QueryPerformanceCounter(&counter.block->start);

#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
            clock_gettime(CLOCK_MONOTONIC, &counter.block->start);

#endif
        }
    }

    return counter;
}

void profiled_block_end(profiler_counter counter)
{
    profiler* p = abe_profiler_ptr;

    if ( !p->disabled && counter.block ) {
#ifdef ABE_WIN
        QueryPerformanceCounter(&counter.block->end);
        
        LARGE_INTEGER freq;
        QueryPerformanceFrequency(&freq);
        counter.block->time += ( counter.block->end.QuadPart - counter.block->start.QuadPart ) * 1000000000 / freq.QuadPart;

#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
        clock_gettime(CLOCK_MONOTONIC, &counter.block->end);
        counter.block->time = (counter.block->end.tv_sec - counter.block->start.tv_sec) * 1e9 + (counter.block->end.tv_nsec - counter.block->start.tv_nsec);

#endif

        p->current = counter.block->parent;
    }
}