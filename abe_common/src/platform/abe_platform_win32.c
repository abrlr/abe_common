#ifdef _WIN64

#include <shlobj_core.h>

#include "abe_common/include/abe_log.h"
#include "abe_common/include/abe_platform.h"
#include "abe_platform_internal.h"

str8 platform_get_local_storage_directory(Platform* platform)
{
	str8 result = (str8)ZeroStruct;
	PlatformInternal* p = (PlatformInternal*)platform;

	PWSTR roamingPathWSTR = 0;
	SHGetKnownFolderPath(&FOLDERID_RoamingAppData, 0, 0, &roamingPathWSTR);

	u32 pathWCharCount = (u32)wcslen(roamingPathWSTR);
	u32 pathCharCount = pathWCharCount * 2 + 1;

	if ( p->arena->occupied + sizeof(char) * pathCharCount >= p->arena->size ) {
		log_err("Not enough storage space in memory arena for local roaming path.\n");
		goto out;
	}

	char* roamingPath = MAFAllocArray(p->arena, char, pathCharCount, "Roaming path");

	size_t convertedCount = 0;
	wcstombs_s(&convertedCount, roamingPath, pathCharCount, roamingPathWSTR, _TRUNCATE);

	result.capacity = pathCharCount;
	result.size = (u32)StringLength(roamingPath);
	result.string = roamingPath;

out:
	if ( roamingPathWSTR )
		CoTaskMemFree(roamingPathWSTR);

	return result;
}

b8 platform_create_directory(str8 directoryPath)
{
	BOOL result = CreateDirectoryA(directoryPath.string, 0);
	if ( result ) return 1;
	return 0;
}

#endif // _WIN64