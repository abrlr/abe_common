#ifndef AbePlatformInternal_h
#define AbePlatformInternal_h

#include "abe_common/include/abe_memory.h"

typedef struct PlatformInternal PlatformInternal;
struct PlatformInternal
{
	MemoryArenaFixed* arena;
};

#endif // AbePlatformInternal_h