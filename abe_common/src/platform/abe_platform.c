#include "abe_common/include/abe_platform.h"
#include "abe_platform_internal.h"

Platform* platform_create(MemoryArenaFixed* arena)
{
	PlatformInternal* result = MAFAllocStruct(arena, PlatformInternal, "abe_Platform");
	result->arena = arena;
	return result;
}
