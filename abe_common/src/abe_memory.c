#include "abe_common/include/abe_memory.h"

void MemoryArenaInit(MemoryArena* arena, void* location, u64 size)
{
    *arena = (MemoryArena){
        .nextEntryID = 0,
        .size = size,
        .occupied = 0,
        .location = location,
        .next = location,
    };

    arena->nextEntry = arena->entries;
}

void MemoryArenaAllocArray(MemoryArena* arena, u8** targetPointerAddress, u64 structSize, u32 count, char* name, char* file, i32 line)
{
    assert(arena->occupied + structSize * count < arena->size);
    u8* location = arena->next;
    arena->occupied += structSize * count;
    arena->next += structSize * count;
    
    arena->nextEntry->id = 1 + arena->nextEntryID++;
    arena->nextEntry->used = 1;
    
#ifdef ABE_DEBUG
    char* lastSlashInFileStr = file + GetLastSlashLoc(file) + 1;
    MemoryZero(arena->nextEntry->file, sizeof(arena->nextEntry->file));
    MemoryZero(arena->nextEntry->name, sizeof(arena->nextEntry->name));
    MemoryCopy(arena->nextEntry->file, (lastSlashInFileStr), StringLength(lastSlashInFileStr));
    MemoryCopy(arena->nextEntry->name, name, StringLength(name));
    arena->nextEntry->line = line;
#endif
    
    arena->nextEntry->size = structSize * count;
    arena->nextEntry->location = (u8*)location;
    arena->nextEntry->pointerAddress = targetPointerAddress;
    arena->nextEntry++;
    
    *targetPointerAddress = location;
}

void MemoryArenaFree(MemoryArena* arena, void* pointer)
{
    for (MemoryArenaEntry* entry = arena->entries; entry < arena->nextEntry; entry++) {
        if ( entry->location == (u8*)pointer ) {
            entry->used = 0;
            arena->occupied -= entry->size;

            //- NOTE(abe): @TODO
            //  Maybe we should make this an option

            //MemoryArenaCollapse(arena);
            break;
        }
    }
}

void MemoryArenaCollapse(MemoryArena* arena)
{
    // Pack memory entries towards the head of the memory arena
    for (u32 i = 0; i < MemoryArenaMaxEntryCount - 1; i++) {
        if ( !arena->entries[i].used && arena->entries[i+1].used ) {
            MemoryCopy(arena->entries[i].location, arena->entries[i+1].location, arena->entries[i+1].size);
            
#ifdef ABE_DEBUG
            MemoryZero(arena->entries[i].file, sizeof(arena->nextEntry->file));
            MemoryZero(arena->entries[i].name, sizeof(arena->nextEntry->name));
            MemoryCopy(arena->entries[i].file, arena->entries[i+1].file, StringLength(arena->entries[i+1].file));
            MemoryCopy(arena->entries[i].name, arena->entries[i+1].name, StringLength(arena->entries[i+1].name));
            arena->entries[i].line = arena->entries[i+1].line;
#endif
            
            arena->entries[i].size = arena->entries[i+1].size;
            arena->entries[i+1].size = 10;
            arena->entries[i].used = 1;
            arena->entries[i+1].used = 0;
            
            arena->entries[i].pointerAddress = arena->entries[i+1].pointerAddress;
            *arena->entries[i].pointerAddress = arena->entries[i].location;
            arena->entries[i+1].location = arena->entries[i].location + arena->entries[i].size;
        }
    }
    
    // Backtrack the allocation cursor
    for (u32 i = MemoryArenaMaxEntryCount - 1; i > 1; i--) {
        if ( !arena->entries[i].used && arena->entries[i - 1].used ) {
            arena->nextEntry = arena->entries + i;
            break;
        }
    }
}

void MemoryArenaClear(MemoryArena* arena)
{
    arena->nextEntry = arena->entries;
    arena->nextEntryID = 0;
    
    arena->next = arena->location;
    arena->occupied = 0;
}

void* MemoryArenaFixedAllocArray(MemoryArenaFixed* arena, u64 structSize, u32 count, char* name, char* file, i32 line)
{
    assert(arena->occupied + structSize * count < arena->size);
    u8* location = arena->next;
    arena->occupied += structSize * count;
    arena->next += structSize * count;
    
    arena->nextEntry->id = 1 + arena->nextEntryID++;
    arena->nextEntry->used = 1;
    
#ifdef ABE_DEBUG
    char* lastSlashInFileStr = file + GetLastSlashLoc(file) + 1;
    MemoryZero(arena->nextEntry->file, sizeof(arena->nextEntry->file));
    MemoryZero(arena->nextEntry->name, sizeof(arena->nextEntry->name));
    MemoryCopy(arena->nextEntry->file, (lastSlashInFileStr), StringLength(lastSlashInFileStr));
    MemoryCopy(arena->nextEntry->name, name, StringLength(name));
    arena->nextEntry->line = line;
#endif
    
    arena->nextEntry->size = structSize * count;
    arena->nextEntry->location = (u8*)location;
    arena->nextEntry->pointerAddress = 0;
    arena->nextEntry++;
    
	return location;
}
