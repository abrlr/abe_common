#ifndef AbeStreamFile_h
#define AbeStreamFile_h

#include "abe_stream_internal.h"

b8 stream_file_close(StreamInternal* stream);

str8 stream_file_read_all(StreamInternal* stream);

b8 stream_file_write_str8(StreamInternal* target, str8 content);

#endif // AbeStreamFile_h