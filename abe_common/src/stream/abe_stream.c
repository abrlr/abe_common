#include "abe_common/include/abe_log.h"

#include "abe_stream_file.h"
#include "abe_stream_string.h"

const char* stream_typename(StreamType type)
{
	switch ( type ) {
		case StreamType_File: return "File";
		case StreamType_String: return "String";
	}

	return "Invalid";
}

b8 stream_close(Stream* stream)
{
	StreamInternal* s = (StreamInternal*)stream;

	switch ( s->type ) {
		case StreamType_File: return stream_file_close(s);
		case StreamType_String: return stream_string_close(s);

		default:
		{
			log_err("Unsupported stream type \"%s\" in stream_close.\n", stream_typename(s->type));
			return 0;
		} break;
	}
}

str8 stream_read_all(Stream* stream)
{
	StreamInternal* s = (StreamInternal*)stream;

	switch ( s->type ) {
		case StreamType_File: return stream_file_read_all(s);
		case StreamType_String: return s->string;

		default:
		{
			log_err("Unsupported stream type \"%s\" in stream_read_all.\n", stream_typename(s->type));
		} break;
	}

	return (str8)ZeroStruct;
}

b8 stream_write_str8(Stream* target, str8 content)
{
	b8 result = 0;
	StreamInternal* s = (StreamInternal*)target;

	if ( !HasFlag(s->attributes, StreamAttributes_Write) ) {
		log_err("Trying to write str8 to readonly stream.\n");
		goto out;
	}

	switch ( s->type ) {
		case StreamType_File: return stream_file_write_str8(s, content);
		case StreamType_String: return stream_string_write_str8(s, content);

		default:
		{
			log_err("Unsupported stream type %d in stream_write_str8.\n", s->type);
			goto out;
		} break;
	}

	result = 1;

out:
	return result;
}