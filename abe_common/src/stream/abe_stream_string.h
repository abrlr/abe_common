#ifndef AbeStreamString_h
#define AbeStreamString_h

#include "abe_stream_internal.h"

b8 stream_string_close(StreamInternal* stream);

b8 stream_string_write_str8(StreamInternal* target, str8 content);

#endif // AbeStreamString_h
