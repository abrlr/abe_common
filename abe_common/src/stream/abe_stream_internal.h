#ifndef AbeStreamInternal_h
#define AbeStreamInternal_h

#include "abe_common/include/abe_stream.h"

typedef enum StreamType_ {
	StreamType_Invalid,

	StreamType_File,
	StreamType_String,

	StreamType_Count,
} StreamType;

const char* stream_typename(StreamType type);

typedef struct StreamInternal StreamInternal;
struct StreamInternal
{
	b8 valid;
	StreamType type;
	StreamAttributes attributes;
	MemoryArenaFixed* arena;

	union {
		void* file;
		str8 string;
	};
};

#endif // AbeStreamInternal_h