#include "abe_common/include/abe_log.h"
#include "abe_stream_string.h"

Stream* stream_create_from_string(MemoryArenaFixed* arena, str8 string)
{
	StreamInternal* result = MAFAllocStruct(arena, StreamInternal, "Stream");
	*result = (StreamInternal)ZeroStruct;
	result->type = StreamType_File;

	result->string.string = MAFAllocArray(arena, char, string.size, "Stream Copy String");
	if ( !result->string.string ) {
		log_err("Failed to allocate copy of string \"%.*s\".\n", string.size, string.string);
		goto out;
	}

	result->string.size = string.size;
	result->string.capacity = string.size;
	MemoryCopy(result->string.string, string.string, result->string.size);

out:
	return result;
}

b8 stream_string_close(StreamInternal* stream)
{
	stream->string = (str8)ZeroStruct;

	return 1;
}

b8 stream_string_write_str8(StreamInternal* target, str8 content)
{
	//- @TODO : implement append to string behavior

	return 1;
}