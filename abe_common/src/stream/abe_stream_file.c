#include <stdio.h>

#include "abe_common/include/abe_log.h"
#include "abe_stream_file.h"

Stream* stream_create_from_file(MemoryArenaFixed* arena, str8 filename, StreamAttributes attributes)
{
	StreamInternal* result = MAFAllocStruct(arena, StreamInternal, "Stream");
	*result = (StreamInternal)ZeroStruct;
	result->type = StreamType_File;
	result->attributes = attributes;
	result->arena = arena;

	FILE* file = 0;
	if ( HasFlag(attributes, StreamAttributes_Read) && !HasFlag(attributes, StreamAttributes_Write) ) {
		file = fopen(filename.string, "r+");
	}

	else if ( !HasFlag(attributes, StreamAttributes_Read) && HasFlag(attributes, StreamAttributes_Write) ) {
		file = fopen(filename.string, "w+");
	}

	else {
		log_err("Unsupported behavior for file stream.\n");
		goto out;
	}
	
	if ( !file ) {
		log_err("Failed to open file %.*s as stream.\n", filename.size, filename.string);
		goto out;
	}

	result->file = (void*)file;

out:
	return result;
}

b8 stream_file_close(StreamInternal* stream)
{
	if ( stream->file ) {
		fclose((FILE*)stream->file);
	}

	return 1;
}

str8 stream_file_read_all(StreamInternal* stream)
{
	str8 result = (str8)ZeroStruct;

	if ( !stream->file ) {
		log_err("Trying to read from ill-initialised stream.\n");
		goto out;
	}

	fseek(stream->file, 0, SEEK_END);
	u32 filesize = ftell(stream->file);
	fseek(stream->file, 0, SEEK_SET);

	if ( !filesize ) {
		log_err("Trying to read data from empty stream.\n");
		goto out;
	}

	result.string = MAFAllocArray(stream->arena, char, filesize, "File Stream Str8 Content");
	if ( !result.string ) {
		log_err("Stream arena is out of memory.\n");
		goto out;
	}

	result.size = filesize;
	result.capacity = filesize;
	fread(result.string, sizeof(char), result.size, stream->file);

out:
	return result;
}

b8 stream_file_write_str8(StreamInternal* target, str8 content)
{
	u32 written = (u32)fwrite(content.string, (u32)content.size, (u32)1, (FILE*)target->file);
	return (written > 0);
}