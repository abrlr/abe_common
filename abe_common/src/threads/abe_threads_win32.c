#ifdef ABE_WIN

#include "abe_common/include/abe_threads.h"

//- [SECTION] Semaphores

thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
{
    thread_semaphore result = CreateSemaphoreEx(0, initialCount, maxCount, 0, 0, SEMAPHORE_ALL_ACCESS);
    return result;
}

i32 thread_semaphore_wait(thread_semaphore* semaphore)
{
    DWORD result = WaitForSingleObject(*semaphore, INFINITE);
    return (i32)( result == 0 );
}

i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)
{
    DWORD result = ReleaseSemaphore(*semaphore, amount, 0);
    return (i32)( result == 0 );
}

//- [SECTION] Threads

thread thread_create(void* startFunction)
{
    thread result = ZeroStruct;
    result.startFunction = startFunction;
    result.state = ThreadState_Invalid;
    return result;
}

void thread_run(thread* thread)
{
    thread->state = ThreadState_Run;
    thread->handle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)thread->startFunction, (void*)thread, 0,  (LPDWORD)&thread->platform_id);
    if ( thread->handle == NULL ) {
        thread->state = ThreadState_Invalid;
    }
}

#endif // ABE_WIN