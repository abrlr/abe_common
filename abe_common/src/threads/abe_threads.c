#include "abe_common/include/abe_threads.h"


//- [SECTION] Work Queue
thread_work_queue thread_work_queue_create(u32 threadCount)
{
    thread_work_queue result = ZeroStruct;
    result.semaphore = thread_semaphore_create(threadCount, threadCount);
    return result;
}

void thread_work_queue_add_entry(thread_work_queue* wq, thread_work_entry_function* userFunction, void* userData)
{
    // NOTE(abe): only one thread can write to the queue 
    // switch to InterlockedCompareExchange so that any thread can add
    
    u32 newNextEntryToWrite = ((wq->nextEntryToWrite + 1) % ArrayCount(wq->entries));
    assert( newNextEntryToWrite != wq->nextEntryToRead);
    thread_work_entry* entry = wq->entries + wq->nextEntryToWrite;
    entry->userData = userData;
    entry->userFunction = userFunction;
    wq->completionGoal++;
    
    COMPLETE_PAST_BEFORE_FUTURE_WRITES;
    
    wq->nextEntryToWrite = newNextEntryToWrite;
    thread_semaphore_release(&wq->semaphore, 1);
}

b8 thread_work_queue_work_exists(thread_work_queue* wq)
{
    return ( wq->nextEntryToRead != wq->nextEntryToWrite );
}

internal b8 thread_internal_complete_next_entry(thread_work_queue* wq, thread* thread)
{
    b8 shouldSleep = 0;
    
    u32 originalNextEntryToRead = wq->nextEntryToRead;
    u32 newNextEntryToRead = ( originalNextEntryToRead + 1 ) % ArrayCount(wq->entries);
    
    if ( originalNextEntryToRead != wq->nextEntryToWrite ) {
        u32 index = InterlockedCompareExchange32(&wq->nextEntryToRead, newNextEntryToRead, originalNextEntryToRead);
        
        if ( index == originalNextEntryToRead ) {
            thread->state = ThreadState_Run;
            thread_work_entry* entry = wq->entries + index;
            entry->userFunction(thread, entry->userData);
            InterlockedIncrement32(&wq->completionCount);
        }
        
    }
    
    else {
        shouldSleep = 1;
    }
    
    return shouldSleep;
}

internal void thread_internal_proc(void* threadData)
{
    thread* t = (thread*)threadData;
    thread_work_queue* queue = t->queue;
    
    for (;;) {
        b8 shouldSleep = thread_internal_complete_next_entry(queue, t);
        if ( shouldSleep ) {
            t->state = ThreadState_Hold;
            thread_semaphore_wait(&queue->semaphore);
        }
    }
}

void thread_spawn(u32 threadCount, thread* threadArray, thread_work_queue* wq)
{
    for (u32 i = 0; i < threadCount; i++) {
        thread* thread = threadArray + i;
        *thread = thread_create((void*)thread_internal_proc);
        thread->queue = wq;
        thread->logical_id = i;
        thread_run(thread);
    }
}

void thread_work_queue_complete(thread_work_queue* wq)
{
    thread dummyThread = ZeroStruct;
    dummyThread.logical_id = U32Max;
    while ( wq->completionCount != wq->completionGoal ) {
        thread_internal_complete_next_entry(wq, &dummyThread);
    }
    
    wq->completionGoal = 0;
    wq->completionCount = 0;
}