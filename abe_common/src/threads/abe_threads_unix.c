#ifndef ABE_WIN

#include "abe_common/include/abe_threads.h"

//- [SECTION] Semaphores

thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount)
{
    thread_semaphore result;
    sem_init(&result, 0, initialCount);
    return result;
}

i32 thread_semaphore_wait(thread_semaphore* semaphore)
{
    sem_wait(semaphore);
    return 1;
}

i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount)
{
    sem_post(semaphore);
    return 1;
}

//- [SECTION] Threads

thread thread_create(void* startFunction) {
    thread result = ZeroStruct;
    result.startFunction = (thread_run_func*)startFunction;
    result.state = ThreadState_Invalid;
    return result;
}

void thread_run(thread* thread) {
    thread->state = ThreadState_Run;
    int valid = pthread_create(&thread->handle, NULL, thread->startFunction, (void*)thread);
    if ( valid != 0 ) {
        thread->state = ThreadState_Invalid;
    }
}


#endif // not ABE_WIN
