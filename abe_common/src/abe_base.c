#include "abe_common/include/abe_base.h"

u32 SafeTruncateU64toU32(u64 size)
{
    assert(size <= 0xFFFFFFFF);
    return (u32)size;
}