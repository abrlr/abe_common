#ifndef AbeBase_h
#define AbeBase_h

//- Multiple defines of static to quickly tell what they are for
#define global static   // persistent global variable
#define internal static // module function
#define local static    // persistent local variable

#define ZeroStruct { 0 }

#define KiloBytes(x) ( (x) * 1024)
#define MegaBytes(x) ( KiloBytes(x) * 1024 )
#define GigaBytes(x) ( MegaBytes(x) * 1024 )

#define Stringify__(x) #x
#define Stringify(x) Stringify__(x)

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#ifdef assert
#undef assert
#endif

#ifdef ABE_WIN
#define WINDOWS_LEAN_AND_MEAN
#include <Windows.h>

#define StructOffset(st, mbr) ( (u32)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { *(int*)0 = 0; } }
#define s_assert(expression, msg) static_assert(expression, msg)
#define m_assert(expression, msg) \
{ \
if ( !(expression) ) { \
char buffer[1024] = { }; \
sprintf(buffer, "%s l.%d: %s", __FILE__, __LINE__, msg); \
MessageBox(NULL, buffer,  "Assert", MB_OK | MB_ICONERROR); \
exit(1); \
} \
}

#define ABE_MAX_PATH (MAX_PATH)

#elif defined(ABE_APPLE)
#define StructOffset(st, mbr) ( (u64)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { __builtin_trap(); } }
#define s_assert(expression, msg)
#define m_assert(expression, msg) (assert(expression))
#define ABE_MAX_PATH (1024 * 4)
#pragma GCC diagnostic ignored "-Wwritable-strings"

#elif defined(ABE_LINUX)
#define StructOffset(st, mbr) ( (u64)(&((st*)0)->mbr) )
#define assert(expression) { if ( !(expression) ) { __builtin_trap(); } }
#define s_assert(expression, msg)
#define m_assert(expression, msg) (assert(expression))
#define ABE_MAX_PATH (1024 * 4)
#pragma GCC diagnostic ignored "-Wwrite-strings"

#endif

//- [SECTION] Types
typedef int8_t b8;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

u32 SafeTruncateU64toU32(u64 size);

//- [SECTION] Limits & Constants
#define I8Max       INT8_MAX
#define I16Max      INT16_MAX
#define I32Max      INT32_MAX
#define I64Max      INT64_MAX
#define U8Max       UINT8_MAX
#define U16Max      UINT16_MAX
#define U32Max      UINT32_MAX
#define U64Max      UINT64_MAX

#define F32Inf      INFINITY
#define F32Pi       3.14159265358979323846f
#define F32e        2.71828182845904523536f
#define F32Epsilon  1e-6

//- [SECTION] Colors
typedef struct color8 color8;
struct color8 {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
};

global color8 color_white       = { .r = 255, .g = 255, .b = 255, .a = 255 };
global color8 color_black       = { .r =   0, .g =   0, .b =   0, .a = 255 };
global color8 color_gray        = { .r = 122, .g = 122, .b = 122, .a = 255 };

global color8 color_red         = { .r = 255, .g =   0, .b =   0, .a = 255 };
global color8 color_green       = { .r =   0, .g = 255, .b =   0, .a = 255 };
global color8 color_blue        = { .r =   0, .g =   0, .b = 255, .a = 255 };

global color8 color_yellow      = { .r = 255, .g = 255, .b =   0, .a = 255 };
global color8 color_pink        = { .r = 255, .g =   0, .b = 255, .a = 255 };
global color8 color_cyan        = { .r =   0, .g = 255, .b = 255, .a = 255 };

//- [SECTION] Flag Utilities
#define BIT(x) ( 1 << x )

#define HasFlag(var, flag)                      (var & flag)
#define SetFlag(var, flag)                      (var |= flag)
#define ClearFlag(var, flag)                    (var &= ~flag)
#define ToggleFlag(var, flag)                   (var ^= flag)

//- [SECTION] Array Utilities
#define AllocArray(type, count)                 (type*)calloc(sizeof(type), count)
#define ArrayCount(array)                       ( sizeof(array) / sizeof((array)[0]) )
#define ArrayCopy(source, target)               { for (int i = 0; i < ArrayCount(source); i++) { target[i] = source[i]; } }
#define ArraySet(array, value)                  { for (int i = 0; i < ArrayCount(array); i++) { array[i] = (value); } }
#define ValueIsInArrayBounds(value, array)      ( value >= array && value < array + ArrayCount(array) )

//- [SECTION] Memory Utilities
#define MemoryCompare(p1, p2, size)             (memcmp((void*)(p1), (void*)(p2), size) == 0)
#define MemoryCopy(dest, from, size)            (memcpy((void*)(dest), (void*)from, size))
#define MemorySet(dest, value, size)            (memset((void*)(dest), value, size))
#define MemoryZero(dest, size)                  (memset((void*)(dest), 0, size))

#endif // AbeBase_h
