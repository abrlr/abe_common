#ifndef AbeTests_h
#define AbeTests_h

#include "abe_log.h"

#define test_assert(expr) { if (!(expr)) { log_err("Test Failed: \"%s\"\n", Stringify(expr)); goto out; } }

#endif // AbeTests_h
