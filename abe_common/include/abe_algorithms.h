#ifndef AbeAlgorithms_h
#define AbeAlgorithms_h

#include "abe_base.h"

//- [SECTION] Sorting Algorithms 

// i32 arrays
void algorithms_i32_quicksort(i32* arr, i32 low, i32 high);

// generic arrays using f32 keys
void algorithms_quicksort(f32* keys, void* values, u32 valueSize, i32 low, i32 high);

#endif // AbeAlgorithms_h
