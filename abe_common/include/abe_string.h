#ifndef AbeString_h
#define AbeString_h

#include "abe_base.h"

#define StringCompare(a, b) (strcmp(a, b) == 0)
#define StringLength(str)   (strlen(str))

//- [SECTION] Char Utilities

b8 IsNumber(char c);
b8 IsAlpha(char c);
b8 IsWhitespace(char c);
b8 IsParenVar(char c);
b8 IsOperator(char c);
b8 IsSpecial(char c);

//- [SECTION] String Utilities

typedef struct str8 str8;
struct str8 {
    u32 size;
	u32 capacity;
    char* string;
};

str8 MakeStr8(const char* string);
b8 AddStr8(str8* dest, str8 toAdd);
b8 ConcatStr8(str8 first, str8 second, str8* dest);

u32 GetLineLength(str8 text);
u32 GetLineCount(str8 text);

char* GetNextLine(str8 text);
char* GetLastSlash(str8 text);

void SplitOnChar(str8 text, char token, str8* splits, u32* splitCount);
void SplitOnWhitespace(str8 text, str8* splits, u32* splitCount);

u32 GetFirstTokenLocation(char token, str8 text);
u32 GetFirstSequenceLocation(const char* seq, str8 text);
u32 GetLastTokenLocation(char token, str8 text);

b8  CompareStrTo(str8 a, const char* b);

//- [SECTION] Number parsing

u32 ReadU32(str8 text);
i32 ReadI32(str8 text);
f32 ReadF32(str8 text);
i32 ReadI32AfterTokenAndAdvance(char* token, str8* text);

#endif // AbeString_h
