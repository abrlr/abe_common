#ifndef AbeLog_h
#define AbeLog_h

#include "abe_base.h"

typedef enum LogLevel_ {
    LogLevel_Msg,
    LogLevel_Info,
    LogLevel_Warn,
    LogLevel_Err,
} LogLevel;

#ifndef ABE_LOG_CONSOLE_ENABLE
void abe_log(LogLevel level, char* file, u32 line, char* format, ...);

#else
#define ABE_LOG_LINE_HEADER_COUNT (32)
#define ABE_LOG_LINE_BODY_COUNT (256)
#define ABE_LOG_CONSOLE_LINE_COUNT (128)

typedef struct log_console_line log_console_line;
struct log_console_line {
    char lineStr[ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT];
    LogLevel level;
};

typedef struct log_console log_console;
struct log_console {
    i32 lineCount;
    log_console_line lines[ABE_LOG_LINE_HEADER_COUNT + ABE_LOG_LINE_BODY_COUNT];
};
log_console* _log_console_ptr;

void abe_log_set_console(log_console* console);
// void abe_log_move_buffer_up();
void abe_log(LogLevel level, char* file, u32 line, char* format,...);

#endif

#ifndef ABE_NO_LOG
#define log_msg(msg, ...) abe_log(LogLevel_Msg, __FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_info(msg, ...) abe_log(LogLevel_Info, __FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_warn(msg, ...) abe_log(LogLevel_Warn, __FILE__, __LINE__, msg, ##__VA_ARGS__)
#define log_err(msg, ...) abe_log(LogLevel_Err, __FILE__, __LINE__, msg, ##__VA_ARGS__)
#else
#define log_msg(msg, ...)
#define log_info(msg, ...)
#define log_warn(msg, ...)
#define log_err(msg, ...)
#endif

#endif // AbeLog_h