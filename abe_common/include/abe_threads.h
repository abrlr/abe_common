#ifndef AbeThreads_h
#define AbeThreads_h

#include "abe_base.h"

//- Forward Declarations

// Structs
typedef struct thread thread;
typedef struct thread_work_entry thread_work_entry;
typedef struct thread_work_queue thread_work_queue;


// Typedefs
typedef u32 ThreadState;

//-

enum ThreadState_ {
    ThreadState_Invalid,
    ThreadState_Run,
    ThreadState_Hold,
    ThreadState_Close
};

#ifdef ABE_WIN

#include <windows.h>
#include <winnt.h>
#include <intrin.h>
#include <process.h>

//- [SECTION] Semaphores
typedef HANDLE thread_semaphore;
thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount);
i32 thread_semaphore_wait(thread_semaphore* semaphore);
i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount);

//- [SECTION] Threads
struct thread {
    HANDLE handle;
    thread_work_queue* queue;
    
    i32 platform_id;
    i32 logical_id;
    
    void* startFunction;
    ThreadState state;
};

thread thread_create(void* startFunction);
void thread_run(thread* thread);

#define COMPLETE_PAST_BEFORE_FUTURE_WRITES _WriteBarrier(); _mm_sfence()
#define COMPLETE_PAST_BEFORE_FUTURE_READS _ReadBarrier()

#define InterlockedIncrement32(ptr) (InterlockedIncrement((LONG volatile*)ptr) - 1)
#define InterlockedCompareExchange32(valueAddr, exchange, comparand) (InterlockedCompareExchange((LONG volatile*)valueAddr, (LONG)exchange, (LONG)comparand))
#define InterlockedExchangePtr(ptrAddr, newPtr) (InterlockedExchangePointer((PVOID*)ptrAddr, (PVOID)newPtr))
#define InterlockedCompareExchangePtr(ptrAddr, exchange, comparand) (InterlockedCompareExchangePointer((PVOID*)ptrAddr, (VOID)exchange, (VOID)comparand))

#elif (defined(ABE_APPLE) || defined(ABE_LINUX))

#include <semaphore.h>
#include <pthread.h>

#define THREADRUNFUNC(name) void* name(void* userData)
typedef THREADRUNFUNC(thread_run_func);

// --- Semaphores ---
typedef sem_t thread_semaphore;
thread_semaphore thread_semaphore_create(i32 initialCount, i32 maxCount);
i32 thread_semaphore_wait(thread_semaphore* semaphore);
i32 thread_semaphore_release(thread_semaphore* semaphore, i32 amount);

// --- Threads ---
struct thread {
    pthread_t handle;
    thread_work_queue* queue;
    
    i32 platform_id;
    i32 logical_id;
    
    thread_run_func* startFunction;
    ThreadState state;
};

thread thread_create(void* startFunction);
void thread_run(thread* thread);

#define COMPLETE_PAST_BEFORE_FUTURE_WRITES //_WriteBarrier(); _mm_sfence()
#define COMPLETE_PAST_BEFORE_FUTURE_READS //_ReadBarrier()

#define InterlockedIncrement32(ptr) __sync_fetch_and_add(ptr, 1)
#define InterlockedCompareExchange32(ptr, exchange, comparand) __sync_val_compare_and_swap(ptr, comparand, exchange)
#define InterlockedExchangePtr(ptr, exchange) __sync_lock_test_and_set(ptr, exchange)

#endif

//- [SECTION] Work Entry
#define thread_work_entry_func(name) void name(thread* thread, void* userData)
typedef thread_work_entry_func(thread_work_entry_function);

struct thread_work_entry {
    thread_work_entry_function* userFunction;
    void* userData;
};

//- [SECTION] Work Queue
struct thread_work_queue {
    thread_semaphore semaphore;
    
    u32 volatile nextEntryToWrite;
    u32 volatile nextEntryToRead;
    
    u32 volatile completionGoal;
    u32 volatile completionCount;
    
    thread_work_entry entries[128];
};

thread_work_queue thread_work_queue_create(u32 threadCount);
void thread_work_queue_add_entry(thread_work_queue* wq, thread_work_entry_function* userFunction, void* userData);
b8 thread_work_queue_work_exists(thread_work_queue* wq);

void thread_spawn(u32 threadCount, thread* threadArray, thread_work_queue* wq);
void thread_work_queue_complete(thread_work_queue* wq);

// internal b8 thread_internal_complete_next_entry(thread_work_queue* wq, thread* thread);
// internal void thread_internal_proc(void* threadData);

#endif // AbeThreads_h
