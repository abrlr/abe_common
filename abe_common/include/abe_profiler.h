#ifndef AbeProfiler_h
#define AbeProfiler_h

#ifdef ABE_WIN
#include <windows.h>
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
#include <time.h>
#else
#error "Unsupported Platform"
#endif

#include "abe_base.h"

//- Forward Declarations

// Structs
typedef struct profiler_block profiler_block;
typedef struct profiler profiler;
typedef struct profiler_counter profiler_counter;

//-

struct profiler_block {
    char name[64];
    char file[64];
    i32 line;
    i32 depth;
    
#ifdef ABE_WIN
    LARGE_INTEGER start;
    LARGE_INTEGER end;
#elif (defined(ABE_APPLE) || defined(ABE_LINUX))
    struct timespec start;
    struct timespec end;
#endif
    
    u64 time; //ns
    
    profiler_block* parent;
    profiler_block* next;
    profiler_block* child;
};

profiler_block* profiler_block_new();

struct profiler {
    b8 disabled;
    u32 count;
    profiler_block blocks[4192];
    profiler_block* next;
    profiler_block* current;
};
extern profiler* abe_profiler_ptr;

void profiler_set(profiler* p);
void profiler_begin_frame();
void profiler_end_frame();
void profiler_dump();

struct profiler_counter {
    profiler_block* block;
};

profiler_counter profiled_block_start(char* name, char* file, int line);
void profiled_block_end(profiler_counter counter);

#define ProfiledBlock(name) profiled_block_start(name, __FILE__, __LINE__)
#define ProfiledBlockEnd(counter) profiled_block_end(counter)

#define ProfiledFunc() profiler_counter function_profiler_counter = ProfiledBlock((char*)__func__)
#define ProfiledFuncEnd() profiled_block_end(function_profiler_counter)

#endif // AbeProfiler_h