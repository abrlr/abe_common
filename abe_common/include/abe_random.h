#ifndef AbeRandom_h
#define AbeRandom_h

#include "abe_base.h"
#include "abe_maths.h"

//- [SECTION] Basic Functionnality
typedef struct random_series random_series;
struct random_series {
    u32 value;
};

random_series random_series_new(u32 seed);
u32 random_u32(random_series* s);
f32 random_f32(random_series* s);
f32 random_f32_in_range(random_series* s, f32 min, f32 max);
i32 random_i32(random_series* s);
i32 random_i32_in_range(random_series* s, i32 min, i32 max);

//- [SECTION] GUIDs
typedef struct guid guid;
struct guid {
    union {
        char bytes[16];
        
        struct {
            struct {
                u8 version : 4;
                u8 variant : 2;
                u64 mBits1 : 58;
            };
            u64 mBits2;
        };
    };
};

guid guid_new(random_series* s);

#endif // AbeRandom_h
