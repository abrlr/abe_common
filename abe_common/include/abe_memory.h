#ifndef AbeMemory_h
#define AbeMemory_h

#include "abe_base.h"

typedef struct MemoryArenaEntry MemoryArenaEntry;
struct MemoryArenaEntry {
    u32 id;
    b8 used;
#ifdef ABE_DEBUG
    char file[32];
    char name[32];
    u32 line;
#endif
    
    u64 size;
    u8* location;
    u8** pointerAddress;
};

#define MemoryArenaMaxEntryCount (2048)
typedef struct MemoryArena MemoryArena;
struct MemoryArena {
    MemoryArenaEntry entries[MemoryArenaMaxEntryCount];
    MemoryArenaEntry* nextEntry;
    u32 nextEntryID;
    
    u64 size;
    u64 occupied;
    u8* location;
    u8* next;
};

void MemoryArenaInit(MemoryArena* arena, void* location, u64 size);
void MemoryArenaAllocArray(MemoryArena* arena, u8** targetPointerAddress, u64 structSize, u32 count, char* name, char* file, i32 line);
void MemoryArenaFree(MemoryArena* arena, void* pointer);
void MemoryArenaCollapse(MemoryArena* arena);
void MemoryArenaClear(MemoryArena* arena);

#define MAAllocArray(arena, pointer, st, count, name) (MemoryArenaAllocArray(arena, (u8**)&pointer, sizeof(st), count, name, __FILE__, __LINE__))
#define MAAllocStruct(arena, pointer, st, name) (MemoryArenaAllocArray(arena, (u8**)&pointer, sizeof(st), 1, name, __FILE__, __LINE__))
#define MAFree(arena, pointer) (MemoryArenaFree(arena, pointer))

typedef struct MemoryArena MemoryArenaFixed;
void* MemoryArenaFixedAllocArray(MemoryArenaFixed* arena, u64 structSize, u32 count, char* name, char* file, i32 line);

#define MAFAllocArray(arena, st, count, name) (MemoryArenaFixedAllocArray(arena, sizeof(st), count, name, __FILE__, __LINE__))
#define MAFAllocStruct(arena, st, name) (MemoryArenaFixedAllocArray(arena, sizeof(st), 1, name, __FILE__, __LINE__))
#define MAFFree(arena, pointer) (MemoryArenaFree(arena, pointer))

#endif // AbeMemory_h
