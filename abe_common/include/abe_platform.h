#ifndef AbePlatform_h
#define AbePlatform_h

#include "abe_memory.h"
#include "abe_string.h"

typedef void Platform;

Platform* platform_create(MemoryArenaFixed* arena);
str8 platform_get_local_storage_directory(Platform* platform);
b8 platform_create_directory(str8 directoryPath);

#endif // AbePlatform_h

