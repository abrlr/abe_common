#ifndef AbeStream_h
#define AbeStream_h

#include "abe_memory.h"
#include "abe_string.h"

typedef enum StreamAttributes_ {
	StreamAttributes_None,
	StreamAttributes_Read = BIT(0),
	StreamAttributes_Write = BIT(1),
	StreamAttributes_ReadWrite = StreamAttributes_Read | StreamAttributes_Write,
} StreamAttributes;

typedef void Stream;

//- Lifetime
Stream* stream_create_from_file(MemoryArenaFixed* arena, str8 filename, StreamAttributes attributes);
Stream* stream_create_from_string(MemoryArenaFixed* arena, str8 string);

b8 stream_close(Stream* stream);

//- Read functions
str8 stream_read_all(Stream* stream);
str8 stream_read_chunk(Stream* stream, u32 size);

//- Write functions
b8 stream_write_str8(Stream* target, str8 content);

#endif // AbeStream_h