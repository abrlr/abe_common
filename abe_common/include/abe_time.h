#ifndef AbeTime_h
#define AbeTime_h

#include "abe_common/include/abe_base.h"
#include "abe_common/include/abe_memory.h"

typedef void Timer;

Timer* timer_create(MemoryArenaFixed* arena);
void timer_start(Timer* timer);
void timer_stop(Timer* timer);

f64 timer_get_elapsed_seconds(Timer* timer);
f64 timer_get_elapsed_miliseconds(Timer* timer);
f64 timer_get_elapsed_microseconds(Timer* timer);

#endif // AbeTime_h