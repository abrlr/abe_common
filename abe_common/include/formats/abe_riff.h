#ifndef AbeFormatsRiff_h
#define AbeFormatsRiff_h

#include "abe_common/include/abe_base.h"

#pragma pack(push, 1)

#define RIFFCHUNKTYPE(a, b, c, d) ( (u32)(a << 0) | (u32)(b << 8) | (u32)(c << 16) | (u32)(d << 24) )

typedef u32 RiffChunkType;
enum RiffChunkType_ {
    RiffChunkType_data = RIFFCHUNKTYPE('d', 'a', 't', 'a'),
    RiffChunkType_fmt = RIFFCHUNKTYPE('f', 'm', 't', ' '),
    RiffChunkType_riff = RIFFCHUNKTYPE('R', 'I', 'F', 'F'),
    RiffChunkType_wave = RIFFCHUNKTYPE('W', 'A', 'V', 'E'),
};

typedef struct riff_chunk_header riff_chunk_header;
struct riff_chunk_header {
    RiffChunkType id;
    u32 size;
};

typedef struct riff_file_iterator riff_file_iterator;
struct riff_file_iterator {
    void* at;
    void* end;
};

RiffChunkType riff_file_iterator_get_chunk_type(riff_file_iterator it);
riff_file_iterator riff_file_iterator_new(void* content, u32 size);
b8 riff_file_iterator_valid(riff_file_iterator* it);
void riff_file_iterate(riff_file_iterator* it);

#pragma pack(pop)

#endif // AbeFormatsRiff_h