#ifndef AbeFormatsJson_h
#define AbeFormatsJson_h

#include "abe_common/include/abe_base.h"
#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_stream.h"

typedef u32 JsonNodeType;
typedef enum JsonNodeType_ {
	JsonNodeType_Invalid,
	JsonNodeType_Number,
	JsonNodeType_Bool,
	JsonNodeType_String,
	JsonNodeType_Array,
	JsonNodeType_Object,
	JsonNodeType_Null,
} JsonNodeType_;

const char* json_typename(JsonNodeType type);

//- These three data structures uses the same logic under the hood
typedef void JsonNode;
typedef JsonNode JsonDocument;
typedef JsonNode JsonArray;

//- Json Documents
JsonDocument* json_document_create(MemoryArenaFixed* arena);
JsonDocument* json_document_create_from_stream(MemoryArenaFixed* arena, Stream* stream);
JsonNode* json_document_create_child(JsonDocument* document, const char* key, JsonNodeType type);
b8 json_document_write_to_stream(JsonDocument* document, Stream* stream, b8 pretty);

//- Json Nodes
JsonNode* json_node_create_child(JsonNode* parent, const char* key, JsonNodeType type);
JsonNodeType json_node_get_type(JsonNode* node);

// Json Node Values
f64 json_node_get_value_as_number(JsonNode* node);
b8 json_node_get_value_as_boolean(JsonNode* node);
str8 json_node_get_value_as_string(JsonNode* node);

// Json Object Nodes
JsonNode* json_node_object_get_field(JsonNode* parent, const char* key);
u32 json_node_object_get_field_count(JsonNode* node);

JsonNode* json_node_add_field_number(JsonNode* node, const char* key, double value);
JsonNode* json_node_add_field_boolean(JsonNode* node, const char* key, b8 value);
JsonNode* json_node_add_field_string(JsonNode* node, const char* key, const char* value);
JsonArray* json_node_add_field_array(JsonNode* node, const char* key, JsonNodeType valueType);

// Json Array Nodes
u32 json_array_get_element_count(JsonArray* array);
JsonNode* json_array_get_node_at(JsonArray* array, u32 at);

f64 json_array_get_number_at(JsonArray* array, u32 at);
b8 json_array_get_boolean_at(JsonArray* array, u32 at);
str8 json_array_get_string_at(JsonArray* array, u32 at);

b8 json_array_append_number(JsonArray* array, double value);
b8 json_array_append_boolean(JsonArray* array, b8 value);
b8 json_array_append_str8(JsonArray* array, str8 value);
b8 json_array_append_string(JsonArray* array, const char* value);
JsonNode* json_array_append_object(JsonArray* array);

//- OLD
JsonNode* JsonParseStr8(MemoryArenaFixed* arena, str8 file);

void JsonPrintNode(JsonNode* entry, i32 level);

#endif // AbeFormatsJson_h
