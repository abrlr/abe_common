#ifndef AbeFormatsWave_h
#define AbeFormatsWave_h

#include "abe_common/include/abe_base.h"
#include "abe_riff.h"

#pragma pack(push, 1)

typedef struct wave_header wave_header;
struct wave_header {
    RiffChunkType riffID;
    u32 fileSize;
    u32 waveID;
};

typedef struct wave_chunk_fmt wave_chunk_fmt;
struct wave_chunk_fmt {
    riff_chunk_header header;
    u16 formatTag;

    u16 channels;
    u32 sampleRate;
    u32 byteRate;
    u16 blockAlign;
    u16 bitsPerSample;
};

typedef struct wave_file wave_file;
struct wave_file {
    // RIFF chunk
    struct {
        char chunkID[4];
        u32 chunkSize;
        char typeID[4];
    };
    
    // WAV Data Description
    struct {
        char dataChunkID[4];
        u32 dataSize;
    };
};

#pragma pack(pop)

#endif // AbeFormatsWave_h
