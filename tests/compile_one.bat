@echo off

if exist bin rmdir /s /q bin
if not exist bin mkdir bin

set CompilerFlags=-DABE_WIN -nologo -I.. -Fo"bin/"
cl %CompilerFlags% src/json_simple.c /link /out:bin/json_simple.exe
