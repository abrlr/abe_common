#/bin/sh

# There is no redirection of the logs in a file here
# This is because we want to keep the log when the tests run on the CI server

White='\e[0m'
Red='\e[0;31m'
Green='\e[0;32m'

ErrorCount=0
TestsCount=0

for file in `find bin/*.app -type f`
do
	filenameandextension="${file##*/}"
	filename="${filenameandextension%.*}"

	start=$(date +%s.%N)
	${file}
	Result=$?
	elapsedString=$(echo "$(date +%s.%N) - ${start}" | bc)
	printf -v duration "%.3f seconds\n" ${elapsedString}

	if [ ${Result} -eq 0 ]; then echo -e ${Green}[SUCCESS]${White} - ${filename} [${duration}];
	else echo -e ${Red}[FAILED]${White} - ${filename} [${duration} ms]; ErrorCount=$((ErrorCount+1));
	fi

	TestsCount=$((TestsCount+1))
done

if ((${ErrorCount}>0)); then echo ${ErrorCount} error/s over ${TestsCount} tests.;
else echo ${TestsCount} tests passed.;
fi
