#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_tests.h"

typedef struct RandomStruct {
    u32 anUnsignedArray[32];
    b8 aBool;
    const char* aPointer;
} RandomStruct;

i32 main(i32 argc, const char** argv)
{
	i32 result = 1;

#define ArenaSize (MegaBytes(32))
    void* memory = calloc(ArenaSize, 1);
    MemoryArena arena = ZeroStruct;
    MemoryArenaInit(&arena, memory, ArenaSize);

	test_assert(arena.size == ArenaSize);

    RandomStruct* structArray = 0;
    MAAllocArray(&arena, structArray, RandomStruct, 128, "");

	test_assert(arena.occupied == sizeof(RandomStruct) * 128);

    RandomStruct* structPointer = 0;
    MAAllocStruct(&arena, structPointer, RandomStruct, "");
	test_assert(arena.occupied == sizeof(RandomStruct) * 129);

    MAFree(&arena, structPointer);

	test_assert(arena.occupied == sizeof(RandomStruct) * 128);

	result = 0;

out:
    return result;
}
