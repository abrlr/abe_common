#include "abe_common/include/abe_maths.h"
#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_tests.h"
#include "abe_common/include/formats/abe_json.h"

i32 main(i32 argc, const char** argv)
{
	i32 result = 1;

	const char* jsonFile = 
	"{"
	"	\"objectArray123\": ["
	"		{"
	"			\"value1\": 100,"
	"			\"value2\": \"Hello\""
	"		},{"
	"			\"value1\": 200,"
	"			\"value2\": \"This is a very complex string.\"" 
	"		}"
	"	]"
	"}";

	u32 arenaSize = MegaBytes(8);
	void* arenaStorage = calloc(arenaSize, 1);
	MemoryArenaFixed arena = ZeroStruct;
	MemoryArenaInit(&arena, arenaStorage, arenaSize);

	JsonNode* root = JsonParseStr8(&arena, MakeStr8(jsonFile));
	if ( !root ) {
		log_err("Failed to parse \"%s\".\n", jsonFile);
		goto out;
	}

	//- Array
	JsonArray* objectArray = json_node_object_get_field(root, "objectArray123");

	//- First entry in the array
	JsonNode* firstNode = json_array_get_node_at(objectArray, 0);
	test_assert(firstNode);

	JsonNodeType firstNodeType = json_node_get_type(firstNode);
	b8 firstNodeIsObject = firstNodeType == JsonNodeType_Object;
	test_assert(firstNodeIsObject);

	u32 firstNodeFieldCount = json_node_object_get_field_count(firstNode);
	b8 firstNodeHas2Fields = firstNodeFieldCount == 2;
	test_assert(firstNodeHas2Fields);

	// First field
	JsonNode* firstNodeValue1 = json_node_object_get_field(firstNode, "value1");
	test_assert(firstNodeValue1);

	JsonNodeType firstNodeValue1Type = json_node_get_type(firstNodeValue1);
	b8 firstNodeValue1IsNumber = firstNodeValue1Type == JsonNodeType_Number;
	test_assert(firstNodeValue1IsNumber);

	f64 firstNodeValue1Number = json_node_get_value_as_number(firstNodeValue1);
	b8 firstNodeValue1Is100 = f32_equals((f32)firstNodeValue1Number, 100.f);
	test_assert(firstNodeValue1Is100);

	// Second field
	JsonNode* firstNodeValue2 = json_node_object_get_field(firstNode, "value2");
	test_assert(firstNodeValue2);

	JsonNodeType firstNodeValue2Type = json_node_get_type(firstNodeValue2);
	b8 firstNodeValue2IsNumber = firstNodeValue2Type == JsonNodeType_String;
	test_assert(firstNodeValue2IsNumber);

	str8 firstNodeValue2String = json_node_get_value_as_string(firstNodeValue2);
	b8 firstNodeValue2IsHello = CompareStrTo(firstNodeValue2String, "Hello");
	test_assert(firstNodeValue2IsHello);

	//- Second entry in the array
	JsonNode* secondNode = json_array_get_node_at(objectArray, 1);
	test_assert(secondNode);

	JsonNodeType secondNodeType = json_node_get_type(secondNode);
	b8 secondNodeIsObject = secondNodeType == JsonNodeType_Object;
	test_assert(secondNodeIsObject);

	u32 secondNodeFieldCount = json_node_object_get_field_count(secondNode);
	b8 secondNodeHas2Fields = secondNodeFieldCount == 2;
	test_assert(secondNodeHas2Fields);

	// second field
	JsonNode* secondNodeValue1 = json_node_object_get_field(secondNode, "value1");
	test_assert(secondNodeValue1);

	JsonNodeType secondNodeValue1Type = json_node_get_type(secondNodeValue1);
	b8 secondNodeValue1IsNumber = secondNodeValue1Type == JsonNodeType_Number;
	test_assert(secondNodeValue1IsNumber);

	f64 secondNodeValue1Number = json_node_get_value_as_number(secondNodeValue1);
	b8 secondNodeValue1Is200 = f32_equals((f32)secondNodeValue1Number, 200.f);
	test_assert(secondNodeValue1Is200);

	// Second field
	JsonNode* secondNodeValue2 = json_node_object_get_field(secondNode, "value2");
	test_assert(secondNodeValue2);

	JsonNodeType secondNodeValue2Type = json_node_get_type(secondNodeValue2);
	b8 secondNodeValue2IsNumber = secondNodeValue2Type == JsonNodeType_String;
	test_assert(secondNodeValue2IsNumber);

	str8 secondNodeValue2String = json_node_get_value_as_string(secondNodeValue2);
	b8 secondNodeValue2IsHello = CompareStrTo(secondNodeValue2String, "This is a very complex string.");
	test_assert(secondNodeValue2IsHello);

	result = 0;

out:
	return result;
}
