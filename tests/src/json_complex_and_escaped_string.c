#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_tests.h"
#include "abe_common/include/formats/abe_json.h"

i32 main(i32 argc, const char** argv)
{
	i32 result = 1;

	const char* jsonFile = 
	"{\n"
	"\t\"a_string\": \"This is a string with non alphanumeric characters ! test@email.com - \\\"This is a question in quotes ??\\\" And these are multiple symbols +-*/()[]{},;:\"\n"
	"}";

	u32 arenaSize = MegaBytes(32);
	void* arenaStorage = calloc(arenaSize, 1);
	MemoryArenaFixed arena = ZeroStruct;
	MemoryArenaInit(&arena, arenaStorage, arenaSize);

	JsonNode* root = JsonParseStr8(&arena, MakeStr8(jsonFile));
	test_assert(root);

	JsonNode* aStringNode = json_node_object_get_field(root, "a_string");
	test_assert(aStringNode);

	JsonNodeType aStringNodeType = json_node_get_type(aStringNode);
	b8 nodeIsString = aStringNodeType == JsonNodeType_String;
	test_assert(nodeIsString);

	const char* expected = "This is a string with non alphanumeric characters ! test@email.com - \\\"This is a question in quotes ??\\\" And these are multiple symbols +-*/()[]{},;:";
	str8 aStringNodeString = json_node_get_value_as_string(aStringNode);
	b8 nodeIsStringValue = CompareStrTo(aStringNodeString, expected);
	test_assert(nodeIsStringValue);

	result = 0;

out:
	return result;
}
