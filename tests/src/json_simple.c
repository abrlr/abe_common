#include "abe_common/include/abe_maths.h"
#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_tests.h"
#include "abe_common/include/formats/abe_json.h"

i32 main(i32 argc, const char** argv)
{
	i32 result = 1;

	const char* jsonFile = "{\n\t\"some_value_1\": 100,\n\t\"some_value_2\": 200\n}";

	u32 arenaSize = MegaBytes(8);
	void* arenaStorage = calloc(arenaSize, 1);
	MemoryArenaFixed arena = ZeroStruct;
	MemoryArenaInit(&arena, arenaStorage, arenaSize);

	JsonNode* root = JsonParseStr8(&arena, MakeStr8(jsonFile));
	test_assert(root);

	JsonNode* someValue1Node = json_node_object_get_field(root, "some_value_1");
	test_assert(someValue1Node);

	JsonNodeType someValue1NodeType = json_node_get_type(someValue1Node);
	b8 nodeIsNumber = someValue1NodeType == JsonNodeType_Number;
	test_assert(nodeIsNumber);

	f64 someValue1NodeNumber = json_node_get_value_as_number(someValue1Node);
	b8 nodeIs100 = f32_equals((f32)someValue1NodeNumber, 100.f);
	test_assert(nodeIs100);

	result = 0;

out:
	return result;
}
