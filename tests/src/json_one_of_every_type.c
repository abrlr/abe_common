#include "abe_common/include/abe_maths.h"
#include "abe_common/include/abe_memory.h"
#include "abe_common/include/abe_tests.h"
#include "abe_common/include/formats/abe_json.h"

i32 main(i32 argc, const char** argv)
{
	i32 result = 1;

	const char* jsonFile = 
		"{\n"
		"	\"test_number\": 1000000,\n"
		"	\"test_string\": \"Hello Sailor !\",\n"
		"	\"test_bool_true\": true,\n"
		"	\"test_bool_false\": false,\n"
		"	\"test_array_numbers\": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],\n"
		"	\"test_object\": { \"randomInt\": 420 },\n"
		"	\"test_null\": null\n"
		"}";

	u32 arenaSize = MegaBytes(8);
	void* arenaStorage = calloc(arenaSize, 1);
	MemoryArenaFixed arena = ZeroStruct;
	MemoryArenaInit(&arena, arenaStorage, arenaSize);

	JsonNode* root = JsonParseStr8(&arena, MakeStr8(jsonFile));
	test_assert(root);

	//- Test Number
	JsonNode* numberNode = json_node_object_get_field(root, "test_number");
	test_assert(numberNode);

	JsonNodeType numberNodeType = json_node_get_type(numberNode);
	b8 numberNodeIsNumber = numberNodeType == JsonNodeType_Number;
	test_assert(numberNodeIsNumber);

	f64 numberNodeNumber = json_node_get_value_as_number(numberNode);
	b8 numberNodeIsAMillion = f32_equals((f32)numberNodeNumber, 1e6f);
	test_assert(numberNodeIsAMillion);

	//- Test String
	JsonNode* stringNode = json_node_object_get_field(root, "test_string");
	test_assert(stringNode);

	JsonNodeType stringNodeType = json_node_get_type(stringNode);
	b8 stringNodeIsString = stringNodeType == JsonNodeType_String;
	test_assert(stringNodeIsString);

	str8 stringNodeString = json_node_get_value_as_string(stringNode);
	b8 stringNodeIsHelloSailor = CompareStrTo(stringNodeString, "Hello Sailor !");
	test_assert(stringNodeIsHelloSailor);

	//- Test Bool True
	JsonNode* boolTrueNode = json_node_object_get_field(root, "test_bool_true");
	test_assert(boolTrueNode);

	JsonNodeType boolTrueNodeType = json_node_get_type(boolTrueNode);
	b8 boolTrueNodeIsBool = boolTrueNodeType == JsonNodeType_Bool;
	test_assert(boolTrueNodeIsBool);

	b8 boolTrueNodeBoolean = json_node_get_value_as_boolean(boolTrueNode);
	b8 boolTrueNodeIsTrue = boolTrueNodeBoolean == 1;
	test_assert(boolTrueNodeIsTrue);

	//- Test Bool False
	JsonNode* boolFalseNode = json_node_object_get_field(root, "test_bool_false");
	test_assert(boolFalseNode);

	JsonNodeType boolFalseNodeType = json_node_get_type(boolFalseNode);
	b8 boolFalseNodeIsBool = boolFalseNodeType == JsonNodeType_Bool;
	test_assert(boolFalseNodeIsBool);

	b8 boolFalseNodeBoolean = json_node_get_value_as_boolean(boolFalseNode);
	b8 boolFalseNodeIsFalse = boolFalseNodeBoolean == 0;
	test_assert(boolFalseNodeIsFalse);

	//- Test Array of Numbers
	JsonNode* arrayNode = json_node_object_get_field(root, "test_array_numbers");
	test_assert(arrayNode);

	u32 entryCount = json_array_get_element_count(arrayNode);
	for (u32 i = 0; i < entryCount; i++) {
		f64 entryNodeNumber = json_array_get_number_at(arrayNode, i);
		b8 entryNodeIsIterator = f32_equals((f32)entryNodeNumber, (f32)i);
		test_assert(entryNodeIsIterator);
	}

	//- Test Object
	JsonNode* objectNode = json_node_object_get_field(root, "test_object");
	test_assert(objectNode);

	JsonNode* objectNodeNode = json_node_object_get_field(objectNode, "randomInt");
	test_assert(objectNodeNode);

	JsonNodeType objectNodeNodeType = json_node_get_type(objectNodeNode);
	b8 objectNodeNodeIsNumber = objectNodeNodeType == JsonNodeType_Number;
	test_assert(objectNodeNodeIsNumber);

	f64 objectNodeNodeNumber = json_node_get_value_as_number(objectNodeNode);
	b8 objectNodeNodeIs420 = f32_equals((f32)objectNodeNodeNumber, 420.f);
	test_assert(objectNodeNodeIs420);

	//- Test Null
	JsonNode* nullNode = json_node_object_get_field(root, "test_null");
	test_assert(nullNode);

	JsonNodeType nullNodeType = json_node_get_type(nullNode);
	b8 nullNodeIsNull = nullNodeType == JsonNodeType_Null;
	test_assert(nullNodeIsNull);


	result = 0;

out:
	return result;
}
