@echo off

setlocal EnableDelayedExpansion

set ASCII27=
set ANSIClear=%ASCII27%[37m
set ANSIRedFG=%ASCII27%[31m
set ANSIGreenFG=%ASCII27%[32m

set ErrorCount=0
set TestsCount=0
set LogFilePath=run.log

set csToMs=10
set secondsToMs=1000
set /a minutesToMs=60*!secondsToMs!
set /a hourToMs=60*!minutesToMs!

echo Last run : %DATE%@%TIME% > %LogFilePath%

for /f %%a in ('dir /b /s "%cd%\bin\*.exe"') do (
	set startTest=!time!

	%%a >> %LogFilePath%

	set endTest=!time!

	set hour=!startTest:~0,2!
	set minutes=!startTest:~3,2!
	set seconds=!startTest:~6,2!
	set cs=!startTest:~9,2!
	set /a startTime=!hour!*!hourToMs! + !minutes! * !minutesToMs! + !seconds! * !secondsToMs! + !cs! * !csToMs!

	set hour=!endTest:~0,2!
	set minutes=!endTest:~3,2!
	set seconds=!endTest:~6,2!
	set cs=!endTest:~9,2!
	set /a endTime=!hour!*!hourToMs! + !minutes! * !minutesToMs! + !seconds! * !secondsToMs! + !cs! * !csToMs!

	set /a elapsed=!endTime!-!startTime!

	if errorlevel 1 (
		echo %ANSIRedFG%[FAILED]%ANSIClear% - %%~na [!elapsed! ms]
		set /a ErrorCount=!ErrorCount!+1
	) else (
		echo %ANSIGreenFG%[SUCCESS]%ANSIClear% - %%~na [!elapsed! ms]
	)

	set /a TestsCount=!TestsCount!+1
)

if %ErrorCount% gtr 0 (
	echo %ErrorCount% error/s over %TestsCount% tests. Please check "%LogFilePath%".
) else (
	echo %TestsCount% tests passed.
	echo %TestsCount% tests passed. >> %LogFilePath%
)

endlocal
