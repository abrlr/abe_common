@echo off

:: Compile the library
pushd ..
call compile.bat
popd

if exist bin rmdir /s /q bin
if not exist bin mkdir bin

:: -W3 -Zi
set CompilerFlags=-DABE_WIN -nologo -I.. -Fo"bin/"

for /f %%a in ('dir /b /s src\*.c') do (
	cl %CompilerFlags% %%a /link ../bin/common.lib /out:bin/%%~na.exe
)

:: -PDB:"bin/%%~na.pdb" -DEBUG
