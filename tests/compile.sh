#/bin/sh

mkdir -p bin

for file in `find src -type f`
do
	filenameandextension="${file##*/}"
	filename="${filenameandextension%.*}"
	echo ${filenameandextension}
	gcc -o bin/${filename}.app -DABE_LINUX -I.. ${file} -L../bin -lcommon -lm -pthread
done
